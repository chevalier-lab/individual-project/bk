<?php
defined('BASEPATH') or exit('No direct script access allowed');

class GroupChat extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");
        $this->load->model("customSQL");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(BK_AUTH)) {
            $this->auth = $this->session->get_session(BK_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }

    public function index()
    {   
        $search = $this->input->post('search', TRUE)['value'];
        $limit = $this->input->post('length', TRUE);
        $start = $this->input->post('start', TRUE);
        $order_index = $this->input->post('order', TRUE)[0]['column'];
        $order_field = $this->input->post('columns', TRUE)[$order_index]['data'];
        $order_ascdesc = $this->input->post('order', TRUE)[0]['dir'];

        $list = $this->customSQL->query("
            SELECT `m_chat_room`.*,
            `konselor`.`full_name` as `konselor_name`,
            `konselor`.`address` as `konselor_address`,
            `konselor`.`phone_number` as `konselor_phone_number` FROM `m_chat_room` 
            JOIN `m_users` as `konselor` ON `konselor`.`id` = `m_chat_room`.`id_master`
            WHERE (`konselor`.`full_name` LIKE '%".$search."%' OR `konselor`.`call_name` LIKE '%".$search."%' OR `konselor`.`address` LIKE '%".$search."%' OR `konselor`.`phone_number` LIKE '%".$search."%' OR `konselor`.`gender` LIKE '%".$search."%' OR `konselor`.`religion` LIKE '%".$search."%' OR `konselor`.`type` LIKE '%".$search."%')
            ORDER BY `m_chat_room`.`updated_at` $order_ascdesc
            LIMIT $limit OFFSET $start
        ")->result_array();

        $total = $this->customSQL->query("
            SELECT COUNT(`m_chat_room`.`id`) AS total FROM `m_chat_room` 
            JOIN `m_users` as `konselor` ON `konselor`.`id` = `m_chat_room`.`id_master`
            WHERE (`konselor`.`full_name` LIKE '%".$search."%' OR `konselor`.`call_name` LIKE '%".$search."%' OR `konselor`.`address` LIKE '%".$search."%' OR `konselor`.`phone_number` LIKE '%".$search."%' OR `konselor`.`gender` LIKE '%".$search."%' OR `konselor`.`religion` LIKE '%".$search."%' OR `konselor`.`type` LIKE '%".$search."%')
            ORDER BY `m_chat_room`.`updated_at` $order_ascdesc
        ")->row()->total;

        die(json_encode(
            array(
                "draw" => $this->input->post("draw", TRUE),
                "recordsTotal" => $total,
                "recordsFiltered" => $total,
                "data" => $list
            )
        ));
    }

    public function remove($id)
    {
        $checkID = $this->customSQL->delete(array("id" => $id), "m_chat_room");

        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal menghapus chat, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil menghapus chat"
            )
        ));
    }

    public function participant($room_id) {
        $list = $this->customSQL->query("
            SELECT `u_chat_room_participant`.*,
            `m_users`.`full_name`,
            `m_users`.`address`,
            `m_users`.`phone_number` FROM `u_chat_room_participant` 
            JOIN `m_users` ON `m_users`.`id` = `u_chat_room_participant`.`id_m_users`
            WHERE `u_chat_room_participant`.`id_room` = '".$room_id."'
            AND `u_chat_room_participant`.`is_accept` = 1
            ORDER BY `u_chat_room_participant`.`updated_at` DESC
        ")->result_array();

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data participant",
                "data" => $list
            )
        ));
    }

    public function detail($id) {
        $list = $this->customSQL->query("
            SELECT `m_chat_room`.* FROM `m_chat_room`
            WHERE `m_chat_room`.`id` = '$id'
        ")->result_array();

        if (count($list) > 0) $list = $list[0];

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data room",
                "data" => $list
            )
        ));
    }

    public function all()
    {   
        $list = $this->customSQL->query("
            SELECT `m_chat_room`.*,
            `u_chat_room_participant`.`is_accept`,
            `konselor`.`full_name` as `konselor_name`,
            `konselor`.`address` as `konselor_address`,
            `konselor`.`phone_number` as `konselor_phone_number` FROM `m_chat_room` 
            JOIN `m_users` as `konselor` ON `konselor`.`id` = `m_chat_room`.`id_master`
            JOIN `u_chat_room_participant` ON `u_chat_room_participant`.`id_room` = `m_chat_room`.`id`
            WHERE `u_chat_room_participant`.`id_m_users` = '".$this->auth->id."'
            GROUP BY `m_chat_room`.`id`
            ORDER BY `m_chat_room`.`updated_at` DESC
        ")->result_array();

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data room",
                "data" => $list
            )
        ));
    }

    public function allKonseli()
    {   
        $list = $this->customSQL->query("
            SELECT `m_chat_room`.*,
            `u_chat_room_participant`.`is_accept`,
            `konselor`.`full_name` as `konselor_name`,
            `konselor`.`address` as `konselor_address`,
            `konselor`.`phone_number` as `konselor_phone_number` FROM `m_chat_room` 
            JOIN `m_users` as `konselor` ON `konselor`.`id` = `m_chat_room`.`id_master`
            JOIN `u_chat_room_participant` ON `u_chat_room_participant`.`id_room` = `m_chat_room`.`id`
            WHERE `u_chat_room_participant`.`id_m_users` = '".$this->auth->id."'
            GROUP BY `m_chat_room`.`id`
            ORDER BY `m_chat_room`.`updated_at` DESC
        ")->result_array();

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data room",
                "data" => $list
            )
        ));
    }

    public function accept($id_room) {
        $data = array(
            "is_accept" => 1,
            "updated_at" => date("Y-m-d H:i:s")
        );

        $checkID = $this->customSQL->update(array(
            "id_room" => $id_room,
            "id_m_users" => $this->auth->id
        ), $data, "u_chat_room_participant");

        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal membuat room, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil bergabung room"
            )
        ));
    }

    public function create()
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["id_master"]) || empty($req["id_master"]) ||
            !isset($req["id_request"]) || empty($req["id_request"]) ||
            !isset($req["title"]) || empty($req["title"]) ||
            !isset($req["description"]) || empty($req["description"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        $data = array(
            "id_master" => $req["id_master"],
            "id_request" => $req["id_request"],
            "title" => $req["title"],
            "description" => $req["description"],
            "token" => md5(date("Y-m-d H:i:s")) . "-" . $req["id_request"] . "-" . $req["id_master"]
        );

        $checkID = $this->customSQL->create($data, "m_chat_room");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal membuat room, terjadi kesalahan disisi server"
            )
        ));

        // Do Create Participant
        $this->customSQL->create(array(
            "id_room" => $checkID,
            "id_m_users" => $req["id_request"],
            "is_accept" => 1
        ), "u_chat_room_participant");
        $this->customSQL->create(array(
            "id_room" => $checkID,
            "id_m_users" => $req["id_master"],
            "is_accept" => 1
        ), "u_chat_room_participant");

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil membuat room"
            )
        ));
    }

    public function invite() {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["id_room"]) || empty($req["id_room"]) ||
            !isset($req["id_m_users"]) || empty($req["id_m_users"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        $checkBefore = $this->customSQL->query("
            SELECT COUNT(`u_chat_room_participant`.`id`) as `total` FROM `u_chat_room_participant` 
            WHERE `id_room` = '".$req["id_room"]."' AND `id_m_users` = '".$req["id_m_users"]."'
        ")->row()->total;
            
        if ($checkBefore > 0) die(json_encode(
            array(
                "code" => 402,
                "message" => "Sudah sudah bergabung sebelumnya"
            )
        ));

        $checkID = $this->customSQL->create(array(
            "id_room" => $req["id_room"],
            "id_m_users" => $req["id_m_users"],
            "is_accept" => 0
        ), "u_chat_room_participant");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal mengundang siswa, terjadi kesalahan disisi server"
            )
        ));

        $id_from = $this->auth->id;
        $id_to = $req["id_m_users"];

        $profile = $this->customSQL->query("
            SELECT * FROM m_users
            WHERE id = '".$id_from."'
        ")->result_array();

        $profile = $profile[0];

        $this->customSQL->create(array(
            "content" => $profile["full_name"] . " mengajak anda untuk bergabung dalam sebuah konseling kelompok",
            "id_from" => $id_from,
            "id_to" => $id_to,
            "params" => base_url("index.php/views/roomChat")
        ), "m_notification");

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil mengundang siswa"
            )
        ));
    }

    public function loadAllChatContent($id)
    {
        $page = $this->input->post_get("page", TRUE) ?: "0";
        $limit = 12;
        $offset = ((int)$page * $limit);

        $list = $this->customSQL->query("
            SELECT `u_chat`.*, `m_users`.`full_name`, 
            `photo`.`uri` as `photo_uri`, `photo`.`file_name` as `photo_file_name`,
            `document`.`uri` as `document_uri`, `document`.`file_name` as `document_file_name` FROM `u_chat` 
            JOIN `m_users` ON `m_users`.`id` = `u_chat`.`id_m_users`
            LEFT JOIN `m_medias` as `photo` ON `photo`.`id` = `u_chat`.`id_photo`
            LEFT JOIN `m_medias` as `document` ON `document`.`id` = `u_chat`.`id_document`
            WHERE `id_room` = '".$id."'
            ORDER BY `u_chat`.`created_at` DESC
            LIMIT $limit OFFSET $offset
        ")->result_array();

        $total = $this->customSQL->query("
            SELECT COUNT(`u_chat`.`id`) as `total` FROM `u_chat` 
            JOIN `m_users` ON `m_users`.`id` = `u_chat`.`id_m_users`
            LEFT JOIN `m_medias` as `photo` ON `photo`.`id` = `u_chat`.`id_photo`
            LEFT JOIN `m_medias` as `document` ON `document`.`id` = `u_chat`.`id_document`
            WHERE `id_room` = '".$id."'
            ORDER BY `u_chat`.`created_at` DESC
        ")->row()->total;

        $sql_filter_count = count($list);

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data chat",
                "data" => $list,
                "meta" => array(
                    "page" => $page,
                    "total_fetch" => $sql_filter_count,
                    "total_data" => $total
                )
            )
        ));
    }

    public function send()
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["id_room"]) || empty($req["id_room"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        $data = array(
            "id_room" => $req["id_room"],
            "id_m_users" => $this->auth->id
        );

        if (isset($req["content"]) && !empty($req["content"])) $data["content"] = $req["content"];
        else if (isset($req["id_document"]) && !empty($req["id_document"])) $data["id_document"] = $req["id_document"];
        else if (isset($req["id_photo"]) && !empty($req["id_photo"])) $data["id_photo"] = $req["id_photo"];
        else die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $checkID = $this->customSQL->create($data, "u_chat");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal mengirim chat, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil mengirim chat"
            )
        ));
    }

}
