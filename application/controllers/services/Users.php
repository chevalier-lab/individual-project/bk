<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Users extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");
        $this->load->model("customSQL");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(BK_AUTH)) {
            $this->auth = $this->session->get_session(BK_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load User List
    public function index()
    {   
        $search = $this->input->post('search', TRUE)['value'];
        $limit = $this->input->post('length', TRUE);
        $start = $this->input->post('start', TRUE);
        $order_index = $this->input->post('order', TRUE)[0]['column'];
        $order_field = $this->input->post('columns', TRUE)[$order_index]['data'];
        $order_ascdesc = $this->input->post('order', TRUE)[0]['dir'];

        $list = $this->customSQL->query("
            SELECT * FROM `m_users` 
            WHERE (`full_name` LIKE '%".$search."%' OR `call_name` LIKE '%".$search."%' OR `address` LIKE '%".$search."%' OR `phone_number` LIKE '%".$search."%' OR `gender` LIKE '%".$search."%' OR `religion` LIKE '%".$search."%' OR `type` LIKE '%".$search."%')
            AND type != 'banned'
            ORDER BY `m_users`.`id` $order_ascdesc
            LIMIT $limit OFFSET $start
        ")->result_array();

        $total = $this->customSQL->query("
            SELECT COUNT(`id`) as `total` FROM `m_users` 
            WHERE (`full_name` LIKE '%".$search."%' OR `call_name` LIKE '%".$search."%' OR `address` LIKE '%".$search."%' OR `phone_number` LIKE '%".$search."%' OR `gender` LIKE '%".$search."%' OR `religion` LIKE '%".$search."%' OR `type` LIKE '%".$search."%')
            AND type != 'banned'
            ORDER BY `m_users`.`id` $order_ascdesc
        ")->row()->total;

        $sql_filter_count = count($list);

        die(json_encode(
            array(
                "draw" => $this->input->post("draw", TRUE),
                "recordsTotal" => $total,
                "recordsFiltered" => $total,
                "data" => $list
            )
        ));
    }

    public function bannedUser($id)
    {
        $checkID = $this->customSQL->update(array("id" => $id), array(
            "type" => "banned"
        ), "m_users");

        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal menghapus akun, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil menghapus akun"
            )
        ));
    }

    public function setUserFace($id)
    {
        $id_m_medias = $this->input->get("id_m_medias", TRUE) ?: "";
        if (empty($id_m_medias)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $checkID = $this->customSQL->update(array("id" => $id), array(
            "id_m_medias" => $id_m_medias
        ), "m_users");

        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal mengubah akun, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil mengubah akun"
            )
        ));
    }

    // Create Admin
    public function createAdmin()
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["full_name"]) || empty($req["full_name"]) ||
            !isset($req["gender"]) || empty($req["gender"]) ||
            !isset($req["religion"]) || empty($req["religion"]) ||
            !isset($req["email"]) || empty($req["email"]) ||
            !isset($req["password"]) || empty($req["password"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        // Check User Already Registered or not
        $checkUserID = $this->customSQL->query("
            SELECT COUNT(id) as total FROM `m_users` 
            WHERE email = '".$req["email"]."'
        ")->row()->total;

        if ($checkUserID > 0) die(json_encode(
            array(
                "code" => 500,
                "message" => "Pengguna sudah terdaftar sebelumnya"
            )
        ));

        // Do Create Basic Users
        $data = array(
            "full_name" => $req["full_name"],
            "call_name" => $req["call_name"],
            "religion" => $req["religion"],
            "email" => $req["email"],
            "password" => $req["password"],
            "type" => "administrator"
        );

        if (isset($req["call_name"]) && !empty($req["call_name"])) $data["call_name"] = $req["call_name"];
        if (isset($req["address"]) && !empty($req["address"])) $data["address"] = $req["address"];
        if (isset($req["phone_number"]) && !empty($req["phone_number"])) $data["phone_number"] = $req["phone_number"];

        $checkID = $this->customSQL->create($data, "m_users");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal membuat akun, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil membuat akun"
            )
        ));
    }

    // Create Conseli
    public function createConseli()
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["full_name"]) || empty($req["full_name"]) ||
            !isset($req["gender"]) || empty($req["gender"]) ||
            !isset($req["religion"]) || empty($req["religion"]) ||
            !isset($req["email"]) || empty($req["email"]) ||
            !isset($req["password"]) || empty($req["password"]) ||

            !isset($req["id_m_class"]) || empty($req["id_m_class"]) || 
            !isset($req["place_of_birth"]) || empty($req["place_of_birth"]) || 
            !isset($req["date_of_birth"]) || empty($req["date_of_birth"]) || 
            !isset($req["nis"]) || empty($req["nis"]) || 
            !isset($req["living_together"]) || empty($req["living_together"]) || 
            !isset($req["distance_from_home"]) || empty($req["distance_from_home"]) || 
            !isset($req["vehicle"]) || empty($req["vehicle"]) || 

            !isset($req["parents"]) || empty($req["parents"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        // Check User Already Registered or not
        $checkUserID = $this->customSQL->query("
            SELECT COUNT(id) as total FROM `m_users` 
            WHERE email = '".$req["email"]."'
        ")->row()->total;

        if ($checkUserID > 0) die(json_encode(
            array(
                "code" => 500,
                "message" => "Pengguna sudah terdaftar sebelumnya"
            )
        ));

        // Do Create Basic Users
        $data = array(
            "full_name" => $req["full_name"],
            "call_name" => $req["call_name"],
            "religion" => $req["religion"],
            "email" => $req["email"],
            "password" => $req["password"],
            "type" => "konseli"
        );

        if (isset($req["call_name"]) && !empty($req["call_name"])) $data["call_name"] = $req["call_name"];
        if (isset($req["address"]) && !empty($req["address"])) $data["address"] = $req["address"];
        if (isset($req["phone_number"]) && !empty($req["phone_number"])) $data["phone_number"] = $req["phone_number"];

        $id_m_users = $this->customSQL->create($data, "m_users");
        
        if ($id_m_users == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal membuat akun, terjadi kesalahan disisi server"
            )
        ));

        $data = array(
            "id_m_users" => $id_m_users,
            "id_m_class" => $req["id_m_class"],
            "nis" => $req["nis"],
            "date_of_birth" => $req["date_of_birth"],
            "place_of_birth" => $req["place_of_birth"],
            "living_together" => $req["living_together"],
            "distance_from_home" => $req["distance_from_home"],
            "vehicle" => $req["vehicle"],
            "child_position" => $req["child_position"],
            "brother_size" => $req["brother_size"],
            "brother_male_size" => $req["brother_male_size"],
            "brother_female_size" => $req["brother_female_size"],
            "litle_brother_male_size" => $req["litle_brother_male_size"],
            "litle_brother_female_size" => $req["litle_brother_female_size"]
        );

        if (isset($req["hobby"]) && !empty($req["hobby"])) $data["hobby"] = $req["hobby"];
        if (isset($req["history_of_disease"]) && !empty($req["history_of_disease"])) $data["history_of_disease"] = $req["history_of_disease"];
        if (isset($req["interest"]) && !empty($req["interest"])) $data["interest"] = $req["interest"];
        if (isset($req["problem"]) && !empty($req["problem"])) $data["problem"] = $req["problem"];

        $checkID = $this->customSQL->create($data, "u_user_student");

        foreach ($req["parents"] as $item) {
            $data = array(
                "id_m_users" => $id_m_users,
                "type" => $item["type"],
                "name" => $item["name"],
                "religion" => $item["religion"],
                "place_of_birth" => $item["place_of_birth"],
                "date_of_birth" => $item["date_of_birth"],
                "job" => $item["job"],
                "address" => $item["address"],
                "phone_number" => $item["phone_number"]
            );

            $checkID = $this->customSQL->create($data, "u_user_student_parent");
        }

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil membuat akun"
            )
        ));
    }

    // Create Conselor
    public function createConselor()
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["full_name"]) || empty($req["full_name"]) ||
            !isset($req["gender"]) || empty($req["gender"]) ||
            !isset($req["religion"]) || empty($req["religion"]) ||
            !isset($req["email"]) || empty($req["email"]) ||
            !isset($req["password"]) || empty($req["password"]) ||

            !isset($req["place_of_birth"]) || empty($req["place_of_birth"]) || 
            !isset($req["date_of_birth"]) || empty($req["date_of_birth"]) || 
            !isset($req["marital_status"]) || empty($req["marital_status"]) || 
            !isset($req["last_education"]) || empty($req["last_education"]) ||
            
            !isset($req["class_names"]) || empty($req["class_names"]) ||
            !isset($req["school_name"]) || empty($req["school_name"]) ||
            !isset($req["school_address"]) || empty($req["school_address"]) ||
            !isset($req["employee_status"]) || empty($req["employee_status"]) ||
            !isset($req["nik"]) || empty($req["nik"]) ||
            !isset($req["year_of_service"]) || empty($req["year_of_service"]) ||
            !isset($req["additional_task"]) || empty($req["additional_task"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        // Check User Already Registered or not
        $checkUserID = $this->customSQL->query("
            SELECT COUNT(id) as total FROM `m_users` 
            WHERE email = '".$req["email"]."'
        ")->row()->total;

        if ($checkUserID > 0) die(json_encode(
            array(
                "code" => 500,
                "message" => "Pengguna sudah terdaftar sebelumnya"
            )
        ));

        // Do Create Basic Users
        $data = array(
            "full_name" => $req["full_name"],
            "call_name" => $req["call_name"],
            "religion" => $req["religion"],
            "email" => $req["email"],
            "password" => $req["password"],
            "type" => "konselor"
        );

        if (isset($req["call_name"]) && !empty($req["call_name"])) $data["call_name"] = $req["call_name"];
        if (isset($req["address"]) && !empty($req["address"])) $data["address"] = $req["address"];
        if (isset($req["phone_number"]) && !empty($req["phone_number"])) $data["phone_number"] = $req["phone_number"];

        $id_m_users = $this->customSQL->create($data, "m_users");
        
        if ($id_m_users == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal membuat akun, terjadi kesalahan disisi server"
            )
        ));

        $data = array(
            "id_m_users" => $id_m_users,
            "place_of_birth" => $req["place_of_birth"],
            "date_of_birth" => $req["date_of_birth"],
            "marital_status" => $req["marital_status"],
            "last_education" => $req["last_education"]
        );
        $checkID = $this->customSQL->create($data, "u_user_konselor");

        $classNames = $req["class_names"];
        foreach ($classNames as $item) {
            $data = array(
                "id_m_users" => $id_m_users,
                "id_m_class" => $item["id"]
            );

            $checkID = $this->customSQL->create($data, "u_user_konselor_class");
        }

        $data = array(
            "id_m_users"=> $id_m_users,
            "school_name"=> $req["school_name"],
            "school_address"=> $req["school_address"],
            "employee_status"=> $req["employee_status"],
            "nik"=> $req["nik"],
            "year_of_service"=> $req["year_of_service"],
            "additional_task"=> $req["additional_task"]
        );

        $checkID = $this->customSQL->create($data, "u_user_konselor_job");

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil membuat akun"
            )
        ));
    }

    public function konselor()
    {
        $list = $this->customSQL->query("
            SELECT `m_users`.`full_name`, `m_users`.`phone_number`, `m_users`.`id`,
            `m_medias`.`uri`
            FROM `m_users` 
            LEFT JOIN `m_medias` ON `m_medias`.`id` = `m_users`.`id_m_medias`
            WHERE `m_users`.`type` = 'konselor'
            ORDER BY `m_users`.`id` DESC
        ")->result_array();

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat konselor",
                "data" => $list
            )
        ));
    }

    public function konseli_by_class($id_m_class)
    {
        $list = $this->customSQL->query("
            SELECT `m_users`.`full_name`, `m_users`.`phone_number`, `m_users`.`id`,
            `m_medias`.`uri`, `u_user_student`.`nis`
            FROM `m_users` 
            LEFT JOIN `m_medias` ON `m_medias`.`id` = `m_users`.`id_m_medias`
            JOIN `u_user_student` ON `u_user_student`.`id_m_users` = `m_users`.`id`
            WHERE `m_users`.`type` = 'konseli'
            ORDER BY `m_users`.`id` DESC
        ")->result_array();

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat konseli",
                "data" => $list
            )
        ));
    }

    // Get
    public function get($id)
    {
        $getUser = $this->customSQL->query("
            SELECT * FROM `m_users` 
            WHERE id = ".$id."
        ")->result_array();

        if (count($getUser) != 1) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser = $getUser[0];

        if ($getUser["type"] != "konseli") die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat pengguna",
                "data" => $getUser
            )
        ));

        $getUserStudent = $this->customSQL->query("
            SELECT * FROM `u_user_student` 
            WHERE id_m_users = ".$id."
        ")->result_array();

        if (count($getUserStudent) != 1) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser["utility"] = $getUserStudent[0];

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat siswa",
                "data" => $getUser
            )
        ));
    }

    // Get
    public function getConseli($id)
    {
        $getUser = $this->customSQL->query("
            SELECT * FROM `m_users` 
            WHERE id = ".$id."
        ")->result_array();

        if (count($getUser) != 1) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser = $getUser[0];

        $getUserStudent = $this->customSQL->query("
            SELECT * FROM `u_user_student` 
            WHERE id_m_users = ".$id."
        ")->result_array();

        if (count($getUserStudent) != 1) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser["utility"] = $getUserStudent[0];

        $getUserStudentParents = $this->customSQL->query("
            SELECT * FROM `u_user_student_parent` 
            WHERE id_m_users = ".$id."
        ")->result_array();

        if (count($getUserStudentParents) == 0) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser["utility"]["parents"] = $getUserStudentParents;

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat siswa",
                "data" => $getUser
            )
        ));
    }

    // Get
    public function getConselor($id)
    {
        $getUser = $this->customSQL->query("
            SELECT * FROM `m_users` 
            WHERE id = ".$id."
        ")->result_array();

        if (count($getUser) != 1) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser = $getUser[0];

        $getUserConselor = $this->customSQL->query("
            SELECT * FROM `u_user_konselor` 
            WHERE id_m_users = ".$id."
        ")->result_array();

        if (count($getUserConselor) != 1) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser["conselor"] = $getUserConselor[0];

        $getUserConselorJob = $this->customSQL->query("
            SELECT * FROM `u_user_konselor_job` 
            WHERE id_m_users = ".$id."
        ")->result_array();

        if (count($getUserConselorJob) != 1) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser["conselor"]["job"] = $getUserConselorJob[0];

        $getUserConselorClass = $this->customSQL->query("
            SELECT `u_user_konselor_class`.*, `m_class`.`id`, `m_class`.`class_name`, `m_class`.`class_code` 
            FROM `u_user_konselor_class` 
            JOIN `m_class` ON `m_class`.`id` = `u_user_konselor_class`.`id_m_class`
            WHERE id_m_users = ".$id."
        ")->result_array();

        if (count($getUserConselorClass) == 0) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser["conselor"]["classes"] = $getUserConselorClass;

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat siswa",
                "data" => $getUser
            )
        ));
    }

    // Update
    public function update($id)
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);

        // Check User Already Registered or not
        $checkUserID = $this->customSQL->query("
            SELECT * FROM `m_users` 
            WHERE id = '".$id."'
        ")->result_array();

        if (count($checkUserID) == 0) die(json_encode(
            array(
                "code" => 500,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $checkUserID = $checkUserID[0];

        // Do Update Basic Users
        $data = array(
            "updated_at" => date("Y-m-d H:i:s")
        );

        if (isset($req["full_name"]) && !empty($req["full_name"])) $data["full_name"] = $req["full_name"];
        if (isset($req["gender"]) && !empty($req["gender"])) $data["gender"] = $req["gender"];
        if (isset($req["religion"]) && !empty($req["religion"])) $data["religion"] = $req["religion"];
        if (isset($req["password"]) && !empty($req["password"])) $data["password"] = $req["password"];
        if (isset($req["call_name"]) && !empty($req["call_name"])) $data["call_name"] = $req["call_name"];
        if (isset($req["address"]) && !empty($req["address"])) $data["address"] = $req["address"];
        if (isset($req["phone_number"]) && !empty($req["phone_number"])) $data["phone_number"] = $req["phone_number"];

        $checkID = $this->customSQL->update(array("id" => $id), $data, "m_users");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal mengubah akun, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil mengubah akun"
            )
        ));
    }

    // Update
    public function updateConselor($id)
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);

        // Check User Already Registered or not
        $checkUserID = $this->customSQL->query("
            SELECT * FROM `m_users` 
            WHERE id = '".$id."'
        ")->result_array();

        if (count($checkUserID) == 0) die(json_encode(
            array(
                "code" => 500,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $checkUserID = $checkUserID[0];

        // Do Update Basic Users
        $data = array(
            "updated_at" => date("Y-m-d H:i:s")
        );

        if (isset($req["full_name"]) && !empty($req["full_name"])) $data["full_name"] = $req["full_name"];
        if (isset($req["gender"]) && !empty($req["gender"])) $data["gender"] = $req["gender"];
        if (isset($req["religion"]) && !empty($req["religion"])) $data["religion"] = $req["religion"];
        if (isset($req["password"]) && !empty($req["password"])) $data["password"] = $req["password"];
        if (isset($req["call_name"]) && !empty($req["call_name"])) $data["call_name"] = $req["call_name"];
        if (isset($req["address"]) && !empty($req["address"])) $data["address"] = $req["address"];
        if (isset($req["phone_number"]) && !empty($req["phone_number"])) $data["phone_number"] = $req["phone_number"];

        $checkID = $this->customSQL->update(array("id" => $id), $data, "m_users");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal mengubah akun, terjadi kesalahan disisi server"
            )
        ));

        $data = array(
            "updated_at" => date("Y-m-d H:i:s"),
            "place_of_birth" => $req["place_of_birth"],
            "date_of_birth" => $req["date_of_birth"],
            "marital_status" => $req["marital_status"],
            "last_education" => $req["last_education"]
        );

        $checkID = $this->customSQL->update(array("id_m_users" => $id), $data, "u_user_konselor");

        $data = array(
            "updated_at" => date("Y-m-d H:i:s"),
            "school_name" => $req["school_name"],
            "school_address" => $req["school_address"],
            "employee_status" => $req["employee_status"],
            "nik" => $req["nik"],
            "year_of_service" => $req["year_of_service"],
            "additional_task" => $req["additional_task"]
        );

        $checkID = $this->customSQL->update(array("id_m_users" => $id), $data, "u_user_konselor_job");

        $this->customSQL->delete(array("id_m_users" => $id), "u_user_konselor_class");

        $classNames = $req["class_names"];
        foreach ($classNames as $item) {
            $data = array(
                "id_m_users" => $id,
                "id_m_class" => $item["id"]
            );

            $checkID = $this->customSQL->create($data, "u_user_konselor_class");
        }

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil mengubah akun"
            )
        ));
    }

    // Update
    public function updateConseli($id)
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);

        // Check User Already Registered or not
        $checkUserID = $this->customSQL->query("
            SELECT * FROM `m_users` 
            WHERE id = '".$id."'
        ")->result_array();

        if (count($checkUserID) == 0) die(json_encode(
            array(
                "code" => 500,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $checkUserID = $checkUserID[0];

        // Do Update Basic Users
        $data = array(
            "updated_at" => date("Y-m-d H:i:s")
        );

        if (isset($req["full_name"]) && !empty($req["full_name"])) $data["full_name"] = $req["full_name"];
        if (isset($req["gender"]) && !empty($req["gender"])) $data["gender"] = $req["gender"];
        if (isset($req["religion"]) && !empty($req["religion"])) $data["religion"] = $req["religion"];
        if (isset($req["password"]) && !empty($req["password"])) $data["password"] = $req["password"];
        if (isset($req["call_name"]) && !empty($req["call_name"])) $data["call_name"] = $req["call_name"];
        if (isset($req["address"]) && !empty($req["address"])) $data["address"] = $req["address"];
        if (isset($req["phone_number"]) && !empty($req["phone_number"])) $data["phone_number"] = $req["phone_number"];

        $checkID = $this->customSQL->update(array("id" => $id), $data, "m_users");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal mengubah akun, terjadi kesalahan disisi server"
            )
        ));

        $data = array(
            "id_m_class" => $req["id_m_class"],
            "nis" => $req["nis"],
            "date_of_birth" => $req["date_of_birth"],
            "place_of_birth" => $req["place_of_birth"],
            "living_together" => $req["living_together"],
            "distance_from_home" => $req["distance_from_home"],
            "vehicle" => $req["vehicle"],
            "child_position" => $req["child_position"],
            "brother_size" => $req["brother_size"],
            "brother_male_size" => $req["brother_male_size"],
            "brother_female_size" => $req["brother_female_size"],
            "litle_brother_male_size" => $req["litle_brother_male_size"],
            "litle_brother_female_size" => $req["litle_brother_female_size"]
        );

        if (isset($req["hobby"]) && !empty($req["hobby"])) $data["hobby"] = $req["hobby"];
        if (isset($req["history_of_disease"]) && !empty($req["history_of_disease"])) $data["history_of_disease"] = $req["history_of_disease"];
        if (isset($req["interest"]) && !empty($req["interest"])) $data["interest"] = $req["interest"];
        if (isset($req["problem"]) && !empty($req["problem"])) $data["problem"] = $req["problem"];

        $checkID = $this->customSQL->update(array("id_m_users" => $id), $data, "u_user_student");

        $this->customSQL->delete(array("id_m_users" => $id), "u_user_student_parent");

        foreach ($req["parents"] as $item) {
            $data = array(
                "id_m_users" => $id,
                "type" => $item["type"],
                "name" => $item["name"],
                "religion" => $item["religion"],
                "place_of_birth" => $item["place_of_birth"],
                "date_of_birth" => $item["date_of_birth"],
                "job" => $item["job"],
                "address" => $item["address"],
                "phone_number" => $item["phone_number"]
            );

            $checkID = $this->customSQL->create($data, "u_user_student_parent");
        }

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil mengubah akun"
            )
        ));
    }

}
