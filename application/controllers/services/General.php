<?php
defined('BASEPATH') or exit('No direct script access allowed');

class General extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");
        $this->load->model("customSQL");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(BK_AUTH)) {
            $this->auth = $this->session->get_session(BK_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load Profile
    public function profile()
    {   
        $id = $this->auth->id;
        $getUser = $this->customSQL->query("
            SELECT `m_users`.*, `m_medias`.`uri` FROM `m_users`
            LEFT JOIN `m_medias` ON `m_users`.`id_m_medias` = `m_medias`.`id` 
            WHERE `m_users`.`id` = ".$id."
        ")->result_array();

        if (count($getUser) != 1) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser = $getUser[0];

        if ($getUser["type"] != "konseli") die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat pengguna",
                "data" => $getUser
            )
        ));

        $getUserStudent = $this->customSQL->query("
            SELECT * FROM `u_user_student` 
            WHERE id_m_users = ".$id."
        ")->result_array();

        if (count($getUserStudent) != 1) die(json_encode(
            array(
                "code" => 404,
                "message" => "Pengguna tidak ditemukan"
            )
        ));

        $getUser["utility"] = $getUserStudent[0];

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat siswa",
                "data" => $getUser
            )
        ));
    }

}
