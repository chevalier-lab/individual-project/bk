<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Portal extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");
        $this->load->model("customSQL");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
    }
    
    // Do Sign In
    public function signIn()
    {
        $raw = $this->input->post_get("raw") ?: "";
        
        if (!empty($raw)) {
            $req = json_decode($raw, TRUE);
            
            // Check Params
            if (!isset($req["user_id"]) || empty($req["user_id"]) ||
                !isset($req["password"]) || empty($req["password"]))
                die(json_encode(
                    array(
                        "code" => 401,
                        "message" => "Parameter tidak benar"
                    )
                ));
            
            $user_id = $req["user_id"];
            $password = $req["password"];

            $check = $this->customSQL->query("
                SELECT `m_users`.* FROM `m_users`
                LEFT JOIN `u_user_student` ON `m_users`.`id` = `u_user_student`.`id_m_users`
                LEFT JOIN `u_user_konselor_job` ON `m_users`.`id` = `u_user_konselor_job`.`id_m_users`
                WHERE (`m_users`.`email` = '".$user_id."' OR `u_user_student`.`nis` = '".$user_id."' OR `u_user_konselor_job`.`nik` = '".$user_id."') AND
                `password` = '".$password."' AND
                `m_users`.`type` != 'banned'
            ")->result_array();

            if (count($check) != 1) die(json_encode(
                array(
                    "code" => 500,
                    "message" => "Akun tidak ditemukan"
                )
            ));

            die(json_encode(
                array(
                    "code" => 200,
                    "data" => $check[0],
                    "message" => "Berhasil melakukan otentikasi"
                )
            ));
        } else die(json_encode(
            array(
                "code" => 401,
                "message" => "Parameter tidak benar"
            )
        ));
    }

}
