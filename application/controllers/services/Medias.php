<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Medias extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;
    public $fileUpload;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");
        $this->load->model("customSQL");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");
        $this->fileUpload = new Upload_file_helper(
            array(
                "file_type" => array(
                    "png",
                    "jpg",
                    "jpeg",
                    "webp",
                    "pdf",
                    "docx",
                    "xls",
                    "xlsx",
                    "doc",
                    "txt"
                ),
                "max_size"  => 200000000
            )
        );

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(BK_AUTH)) {
            $this->auth = $this->session->get_session(BK_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    public function create()
    {
        if (!isset($_FILES["photo"]["name"])) 
        die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $photo = $this->fileUpload->do_upload("photo");

        if (!$photo["status"])
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal menyimpan gambar, terjadi kesalahan disisi server"
            )
        ));

        // Upload File
        $data = array(
            "uri" => base_url("assets/dist/img/") . $photo["file_name"],
            "file_name" => $photo["file_name"],
            "created_at" => date("Y-m-d H:i:s"),
            "updated_at" => date("Y-m-d H:i:s")
        );

        $checkID = $this->customSQL->create($data, "m_medias");

        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal membuat akun, terjadi kesalahan disisi server"
            )
        ));

        $data["id"] = $checkID;

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil membuat media", 
                "data" => $data
            )
        ));
    }

    public function remove()
    {   
        $src = $this->input->post("src", TRUE) ?: "";

        if (empty($src)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $getMedia = $this->customSQL->query("
            SELECT * FROM `m_medias` 
            WHERE uri = '".$src."'
        ")->result_array();

        if (count($getMedia) != 1) die(json_encode(
            array(
                "code" => 500,
                "message" => "Media tidak ditemukan"
            )
        ));

        $getMedia = $getMedia[0];

		$checkID = $this->customSQL->delete(array("id" => $getMedia["id"]), "m_medias");

        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal menghapus akun, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil menghapus media"
            )
        ));
    }

}
