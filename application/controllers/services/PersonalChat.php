<?php
defined('BASEPATH') or exit('No direct script access allowed');

class PersonalChat extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");
        $this->load->model("customSQL");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(BK_AUTH)) {
            $this->auth = $this->session->get_session(BK_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }

    public function index()
    {
        $search = $this->input->post('search', TRUE)['value'];
        $limit = $this->input->post('length', TRUE);
        $start = $this->input->post('start', TRUE);
        $order_index = $this->input->post('order', TRUE)[0]['column'];
        $order_field = $this->input->post('columns', TRUE)[$order_index]['data'];
        $order_ascdesc = $this->input->post('order', TRUE)[0]['dir'];
        
        $list = $this->customSQL->query("
            SELECT `m_personal_chat_room`.*,
            `konseli`.`full_name` as `konseli_name`,
            `konselor`.`full_name` as `konselor_name` FROM `m_personal_chat_room` 
            JOIN `m_users` as `konseli` ON `konseli`.`id` = `m_personal_chat_room`.`id_konseli`
            JOIN `m_users` as `konselor` ON `konselor`.`id` = `m_personal_chat_room`.`id_konselor`
            WHERE (`konseli`.`full_name` LIKE '%".$search."%' OR `konseli`.`call_name` LIKE '%".$search."%' OR `konseli`.`address` LIKE '%".$search."%' OR `konseli`.`phone_number` LIKE '%".$search."%' OR `konseli`.`gender` LIKE '%".$search."%' OR `konseli`.`religion` LIKE '%".$search."%' OR `konseli`.`type` LIKE '%".$search."%')
            OR (`konselor`.`full_name` LIKE '%".$search."%' OR `konselor`.`call_name` LIKE '%".$search."%' OR `konselor`.`address` LIKE '%".$search."%' OR `konselor`.`phone_number` LIKE '%".$search."%' OR `konselor`.`gender` LIKE '%".$search."%' OR `konselor`.`religion` LIKE '%".$search."%' OR `konselor`.`type` LIKE '%".$search."%')
            ORDER BY `m_personal_chat_room`.`updated_at` $order_ascdesc
            LIMIT $limit OFFSET $start
        ")->result_array();

        $total = $this->customSQL->query("
            SELECT COUNT(`m_personal_chat_room`.`id`) AS total FROM `m_personal_chat_room` 
            JOIN `m_users` as `konseli` ON `konseli`.`id` = `m_personal_chat_room`.`id_konseli`
            JOIN `m_users` as `konselor` ON `konselor`.`id` = `m_personal_chat_room`.`id_konselor`
            WHERE (`konseli`.`full_name` LIKE '%".$search."%' OR `konseli`.`call_name` LIKE '%".$search."%' OR `konseli`.`address` LIKE '%".$search."%' OR `konseli`.`phone_number` LIKE '%".$search."%' OR `konseli`.`gender` LIKE '%".$search."%' OR `konseli`.`religion` LIKE '%".$search."%' OR `konseli`.`type` LIKE '%".$search."%')
            OR (`konselor`.`full_name` LIKE '%".$search."%' OR `konselor`.`call_name` LIKE '%".$search."%' OR `konselor`.`address` LIKE '%".$search."%' OR `konselor`.`phone_number` LIKE '%".$search."%' OR `konselor`.`gender` LIKE '%".$search."%' OR `konselor`.`religion` LIKE '%".$search."%' OR `konselor`.`type` LIKE '%".$search."%')
            ORDER BY `m_personal_chat_room`.`updated_at` $order_ascdesc
        ")->row()->total;

        die(json_encode(
            array(
                "draw" => $this->input->post("draw", TRUE),
                "recordsTotal" => $total,
                "recordsFiltered" => $total,
                "data" => $list
            )
        ));
    }

    public function remove($id)
    {
        $checkID = $this->customSQL->delete(array("id" => $id), "m_personal_chat_room");

        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal menghapus chat, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil menghapus chat"
            )
        ));
    }

    public function all()
    {   
        $list = $this->customSQL->query("
            SELECT `m_personal_chat_room`.*,
            `konseli`.`full_name` as `konseli_name`,
            `konseli`.`address` as `konseli_address`,
            `konseli`.`phone_number` as `konseli_phone_number`,
            `konselor`.`full_name` as `konselor_name`,
            `konselor`.`address` as `konselor_address`,
            `konselor`.`phone_number` as `konselor_phone_number` FROM `m_personal_chat_room` 
            JOIN `m_users` as `konseli` ON `konseli`.`id` = `m_personal_chat_room`.`id_konseli`
            JOIN `m_users` as `konselor` ON `konselor`.`id` = `m_personal_chat_room`.`id_konselor`
            WHERE `konseli`.`id` = '". $this->auth->id ."'
            ORDER BY `m_personal_chat_room`.`updated_at` DESC
        ")->result_array();

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data room",
                "data" => $list
            )
        ));
    }

    public function allKonseli()
    {   
        $list = $this->customSQL->query("
            SELECT `m_personal_chat_room`.*,
            `konseli`.`full_name` as `konseli_name`,
            `konseli`.`address` as `konseli_address`,
            `konseli`.`phone_number` as `konseli_phone_number`,
            `konselor`.`full_name` as `konselor_name`,
            `konselor`.`address` as `konselor_address`,
            `konselor`.`phone_number` as `konselor_phone_number` FROM `m_personal_chat_room` 
            JOIN `m_users` as `konseli` ON `konseli`.`id` = `m_personal_chat_room`.`id_konseli`
            JOIN `m_users` as `konselor` ON `konselor`.`id` = `m_personal_chat_room`.`id_konselor`
            WHERE `konselor`.`id` = '". $this->auth->id ."'
            ORDER BY `m_personal_chat_room`.`updated_at` DESC
        ")->result_array();

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data room",
                "data" => $list
            )
        ));
    }

    public function create()
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["id_konselor"]) || empty($req["id_konselor"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        $checkRoom = $this->customSQL->query("
            SELECT COUNT(`m_personal_chat_room`.`id`) as total FROM `m_personal_chat_room` 
            WHERE `id_konseli` = '". $this->auth->id ."' AND `id_konselor` = '". $req["id_konselor"] ."'
        ")->row()->total;

        if ($checkRoom > 0)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Room sudah ada sebelumnya"
            )
        ));

        $data = array(
            "id_konselor" => $req["id_konselor"],
            "id_konseli" => $this->auth->id,
            "token" => md5(date("Y-m-d H:i:s")) . "-" . $this->auth->id . "-" . $req["id_konselor"]
        );

        $checkID = $this->customSQL->create($data, "m_personal_chat_room");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal membuat room, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil membuat room"
            )
        ));
    }

    public function loadAllChatContent($id)
    {
        $page = $this->input->post_get("page", TRUE) ?: "0";
        $limit = 12;
        $offset = ((int)$page * $limit);

        $list = $this->customSQL->query("
            SELECT `u_personal_chat`.*, `m_users`.`full_name`, 
            `photo`.`uri` as `photo_uri`, `photo`.`file_name` as `photo_file_name`,
            `document`.`uri` as `document_uri`, `document`.`file_name` as `document_file_name` FROM `u_personal_chat` 
            JOIN `m_users` ON `m_users`.`id` = `u_personal_chat`.`id_m_users`
            LEFT JOIN `m_medias` as `photo` ON `photo`.`id` = `u_personal_chat`.`id_photo`
            LEFT JOIN `m_medias` as `document` ON `document`.`id` = `u_personal_chat`.`id_document`
            WHERE `id_room` = '".$id."'
            ORDER BY `u_personal_chat`.`created_at` DESC
            LIMIT $limit OFFSET $offset
        ")->result_array();

        $total = $this->customSQL->query("
            SELECT COUNT(`u_personal_chat`.`id`) as `total` FROM `u_personal_chat` 
            JOIN `m_users` ON `m_users`.`id` = `u_personal_chat`.`id_m_users`
            LEFT JOIN `m_medias` as `photo` ON `photo`.`id` = `u_personal_chat`.`id_photo`
            LEFT JOIN `m_medias` as `document` ON `document`.`id` = `u_personal_chat`.`id_document`
            WHERE `id_room` = '".$id."'
            ORDER BY `u_personal_chat`.`created_at` DESC
        ")->row()->total;

        $sql_filter_count = count($list);

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data chat",
                "data" => $list,
                "meta" => array(
                    "page" => $page,
                    "total_fetch" => $sql_filter_count,
                    "total_data" => $total
                )
            )
        ));
    }

    public function send()
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["id_room"]) || empty($req["id_room"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        $data = array(
            "id_room" => $req["id_room"],
            "id_m_users" => $this->auth->id
        );

        $pesan = "Anda mendapatkan sebuah pesan ";

        if (isset($req["content"]) && !empty($req["content"])) $data["content"] = $req["content"];
        else if (isset($req["id_document"]) && !empty($req["id_document"])) {
            $pesan .= "dokumen";
            $data["id_document"] = $req["id_document"];
        }
        else if (isset($req["id_photo"]) && !empty($req["id_photo"])) {
            $pesan .= "gambar";
            $data["id_photo"] = $req["id_photo"];
        }
        else die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $checkID = $this->customSQL->create($data, "u_personal_chat");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal mengirim chat, terjadi kesalahan disisi server"
            )
        ));

        $room = $this->customSQL->query("
            SELECT * FROM m_personal_chat_room
            WHERE id = '".$req["id_room"]."'
        ")->result_array();

        $room = $room[0];

        if ($room["id_konseli"] == $this->auth->id) {
            $id_from = $this->auth->id;
            $id_to = $room["id_konselor"];

            $profile = $this->customSQL->query("
                SELECT * FROM m_users
                WHERE id = '".$room["id_konseli"]."'
            ")->result_array();

            $profile = $profile[0];

            $this->customSQL->create(array(
                "content" => $pesan . " dari " . $profile["full_name"],
                "id_from" => $id_from,
                "id_to" => $id_to,
                "params" => base_url("index.php/views/personalChatConselor")
            ), "m_notification");
        } else {
            $id_from = $this->auth->id;
            $id_to = $room["id_konseli"];

            $profile = $this->customSQL->query("
                SELECT * FROM m_users
                WHERE id = '".$room["id_konselor"]."'
            ")->result_array();

            $profile = $profile[0];

            $this->customSQL->create(array(
                "content" => $pesan . " dari " . $profile["full_name"],
                "id_from" => $id_from,
                "id_to" => $id_to,
                "params" => base_url("index.php/views/personalChat")
            ), "m_notification");
        }

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil mengirim chat"
            )
        ));
    }

}
