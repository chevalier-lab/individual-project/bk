<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notification extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");
        $this->load->model("customSQL");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(BK_AUTH)) {
            $this->auth = $this->session->get_session(BK_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load Class List
    public function index()
    {   
        $search = $this->input->post('search', TRUE)['value'];
        $limit = $this->input->post('length', TRUE);
        $start = $this->input->post('start', TRUE);
        $order_index = $this->input->post('order', TRUE)[0]['column'];
        $order_field = $this->input->post('columns', TRUE)[$order_index]['data'];
        $order_ascdesc = $this->input->post('order', TRUE)[0]['dir'];

        $list = $this->customSQL->query("
            SELECT * FROM `m_notification` 
            WHERE (`content` LIKE '%".$search."%')
            AND id_to = '".$this->auth->id."'
            ORDER BY `created_at`.`id` $order_ascdesc
            LIMIT $limit OFFSET $start
        ")->result_array();

        $total = $this->customSQL->query("
            SELECT COUNT(`id`) as `total` FROM `m_notification` 
            WHERE (`content` LIKE '%".$search."%')
            AND id_to = '".$this->auth->id."'
            ORDER BY `created_at`.`id` $order_ascdesc
        ")->row()->total;

        $sql_filter_count = count($list);

        die(json_encode(
            array(
                "draw" => $this->input->post("draw", TRUE),
                "recordsTotal" => $total,
                "recordsFiltered" => $sql_filter_count,
                "data" => $list
            )
        ));
    }

    public function all()
    {   
        $list = $this->customSQL->query("
            SELECT * FROM `m_notification` 
            WHERE id_to = '".$this->auth->id."'
            ORDER BY `created_at` DESC
        ")->result_array();

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data notification",
                "data" => $list
            )
        ));
    }

}
