<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Category extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");
        $this->load->model("customSQL");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(BK_AUTH)) {
            $this->auth = $this->session->get_session(BK_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load Class List
    public function index()
    {   
        $search = $this->input->post('search', TRUE)['value'];
        $limit = $this->input->post('length', TRUE);
        $start = $this->input->post('start', TRUE);
        $order_index = $this->input->post('order', TRUE)[0]['column'];
        $order_field = $this->input->post('columns', TRUE)[$order_index]['data'];
        $order_ascdesc = $this->input->post('order', TRUE)[0]['dir'];

        $list = $this->customSQL->query("
            SELECT * FROM `m_categories` 
            WHERE (`category` LIKE '%".$search."%')
            ORDER BY `m_categories`.`id` $order_ascdesc
            LIMIT $limit OFFSET $start
        ")->result_array();

        $total = $this->customSQL->query("
            SELECT COUNT(`id`) as `total` FROM `m_categories` 
            WHERE (`category` LIKE '%".$search."%')
            ORDER BY `m_categories`.`id` $order_ascdesc
        ")->row()->total;

        $sql_filter_count = count($list);

        die(json_encode(
            array(
                "draw" => $this->input->post("draw", TRUE),
                "recordsTotal" => $total,
                "recordsFiltered" => $sql_filter_count,
                "data" => $list
            )
        ));
    }

    public function all()
    {   
        $list = $this->customSQL->query("
            SELECT * FROM `m_categories` 
            ORDER BY `category` DESC
        ")->result_array();

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data kategori",
                "data" => $list
            )
        ));
    }

    public function create()
    {
        $raw = $this->input->post("raw", TRUE) ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["category"]) || empty($req["category"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        $data = array(
            "category" => $req["category"]
        );

        $checkID = $this->customSQL->create($data, "m_categories");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal membuat kategori, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil membuat kategori"
            )
        ));
    }

}
