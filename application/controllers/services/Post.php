<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Post extends CI_Controller
{
    // Public Variable
    public $session, $custom_curl;
    public $csrf_token, $auth;
    public $topBarContent, $navBarContent;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("request");
        $this->load->model("customSQL");

        // Load Helper
        $this->session = new Session_helper();
        $this->custom_curl = new Mycurl_helper("");

        // Check Tokenize
        if (!$this->tokenize->isValid()) {
            die(json_encode(
                array(
                    "code" => 401,
                    "message" => "Unauthorized, butuh csrf_token"
                )
            ));
        }

        // Init Request
        $this->request->init($this->custom_curl);
    }

    private function checkAuth() {
        if ($this->session->check_session(BK_AUTH)) {
            $this->auth = $this->session->get_session(BK_AUTH);
        } else die(json_encode(
            array(
                "code" => 500,
                "message" => "Tidak terotentikasi"
            )
        ));
    }
    
    // Load Class List
    public function index()
    {   
        $this->checkAuth();

        $search = $this->input->post('search', TRUE)['value'];
        $limit = $this->input->post('length', TRUE);
        $start = $this->input->post('start', TRUE);
        $order_index = $this->input->post('order', TRUE)[0]['column'];
        $order_field = $this->input->post('columns', TRUE)[$order_index]['data'];
        $order_ascdesc = $this->input->post('order', TRUE)[0]['dir'];

        $list = $this->customSQL->query("
            SELECT `m_post`.*, `m_users`.`full_name`, `m_categories`.`category`, `m_medias`.`uri` FROM `m_post` 
            JOIN `m_users` ON `m_users`.`id` = `m_post`.`id_m_users`
            JOIN `m_categories` ON `m_categories`.`id` = `m_post`.`id_m_categories`
            JOIN `m_medias` ON `m_medias`.`id` = `m_post`.`id_m_medias`
            WHERE (`title` LIKE '%".$search."%' OR `content` LIKE '%".$search."%')
            AND `id_m_users` = '".$this->auth->id."'
            ORDER BY `m_post`.`id` $order_ascdesc
            LIMIT $limit OFFSET $start
        ")->result_array();

        $total = $this->customSQL->query("
            SELECT COUNT(`m_post`.`id`) as `total` FROM `m_post` 
            JOIN `m_users` ON `m_users`.`id` = `m_post`.`id_m_users`
            JOIN `m_categories` ON `m_categories`.`id` = `m_post`.`id_m_categories`
            JOIN `m_medias` ON `m_medias`.`id` = `m_post`.`id_m_medias`
            WHERE (`title` LIKE '%".$search."%' OR `content` LIKE '%".$search."%')
            AND `id_m_users` = '".$this->auth->id."'
            ORDER BY `m_post`.`id` $order_ascdesc
        ")->row()->total;

        $sql_filter_count = count($list);

        die(json_encode(
            array(
                "draw" => $this->input->post("draw", TRUE),
                "recordsTotal" => $total,
                "recordsFiltered" => $sql_filter_count,
                "data" => $list
            )
        ));
    }

    public function all()
    {   
        $page = $this->input->post_get("page", TRUE) ?: "0";
        $search = $this->input->post_get("search", TRUE) ?: "";
        $limit = 12;
        $offset = ((int)$page * $limit);

        $list = $this->customSQL->query("
            SELECT `m_post`.*, `m_users`.`full_name`, `m_categories`.`category`, `m_medias`.`uri` FROM `m_post` 
            JOIN `m_users` ON `m_users`.`id` = `m_post`.`id_m_users`
            JOIN `m_categories` ON `m_categories`.`id` = `m_post`.`id_m_categories`
            JOIN `m_medias` ON `m_medias`.`id` = `m_post`.`id_m_medias`
            WHERE (`title` LIKE '%".$search."%' OR `content` LIKE '%".$search."%')
            ORDER BY `m_post`.`created_at` DESC
            LIMIT $limit OFFSET $offset
        ")->result_array();

        $total = $this->customSQL->query("
            SELECT COUNT(`m_post`.`id`) as `total` FROM `m_post` 
            JOIN `m_users` ON `m_users`.`id` = `m_post`.`id_m_users`
            JOIN `m_categories` ON `m_categories`.`id` = `m_post`.`id_m_categories`
            JOIN `m_medias` ON `m_medias`.`id` = `m_post`.`id_m_medias`
            WHERE (`title` LIKE '%".$search."%' OR `content` LIKE '%".$search."%')
            ORDER BY `m_post`.`created_at` DESC
        ")->row()->total;

        $sql_filter_count = count($list);

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data post",
                "data" => $list,
                "meta" => array(
                    "page" => $page,
                    "search" => $search,
                    "total_fetch" => $sql_filter_count,
                    "total_data" => $total
                )
            )
        ));
    }

    public function detail($id_post)
    {   
        $this->checkAuth();

        $list = $this->customSQL->query("
            SELECT `m_post`.*, `m_users`.`full_name`, `m_categories`.`category`, `m_medias`.`uri` FROM `m_post` 
            JOIN `m_users` ON `m_users`.`id` = `m_post`.`id_m_users`
            JOIN `m_categories` ON `m_categories`.`id` = `m_post`.`id_m_categories`
            JOIN `m_medias` ON `m_medias`.`id` = `m_post`.`id_m_medias`
            AND `m_post`.`id` = '".$id_post."'
        ")->result_array();

        if (count($list) > 0) $list = $list[0];
        else die(json_encode(
            array(
                "code" => 400,
                "message" => "Artikel tidak ditemukan"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil memuat data post",
                "data" => $list
            )
        ));
    }

    public function create()
    {
        $this->checkAuth();
        $raw = $this->input->post("raw") ?: "";

        if (empty($raw)) die(json_encode(
            array(
                "code" => 400,
                "message" => "Parameter tidak benar"
            )
        ));

        $req = json_decode($raw, TRUE);
        if (!isset($req["title"]) || empty($req["title"]) ||
            !isset($req["content"]) || empty($req["content"]) ||
            !isset($req["id_m_categories"]) || empty($req["id_m_categories"]) ||
            !isset($req["id_m_medias"]) || empty($req["id_m_medias"])) 
            die(json_encode(
                array(
                    "code" => 400,
                    "message" => "Parameter tidak benar"
                )
            ));

        $data = array(
            "title" => $req["title"],
            "content" => $req["content"],
            "id_m_users" => $this->auth->id,
            "id_m_categories" => $req["id_m_categories"],
            "id_m_medias" => $req["id_m_medias"]
        );

        $checkID = $this->customSQL->create($data, "m_post");
        
        if ($checkID == -1)
        die(json_encode(
            array(
                "code" => 500,
                "message" => "Gagal membuat artikel, terjadi kesalahan disisi server"
            )
        ));

        die(json_encode(
            array(
                "code" => 200,
                "message" => "Berhasil membuat artikel"
            )
        ));
    }

}
