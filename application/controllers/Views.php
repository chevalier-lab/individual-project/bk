<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Views extends CI_Controller
{
    // Public Variable
    public $session;
    public $csrf_token, $auth;
    public $menu;

    public function __construct()
    {
        parent::__construct();

        // Load Model
        $this->load->model("tokenize");
        $this->load->model("sidebar");

        // Load Helper
        $this->session = new Session_helper();        

        // Generate Valid Tokenize
        $this->csrf_token = $this->tokenize->generate();
        $this->checkAuth();
    }

    private function checkAuth() {
        if ($this->session->check_session(BK_AUTH)) {
            $this->auth = $this->session->get_session(BK_AUTH);
            switch ($this->auth->type) {
                case "administrator": $this->menu = $this->sidebar->administrator();
                break;
                case "konseli": $this->menu = $this->sidebar->konseli();
                break;
                case "konselor": $this->menu = $this->sidebar->konselor();
                break;
                default: $this->menu = $this->sidebar->default();
                break;
            }

            // print_r($this->auth);
            // die();
        } else $this->menu = $this->sidebar->default();
    }

    // ------------------------------ PORTAL
    
    // Index
    public function index()
    {
        $this->home();
    }

    // Sign In
    public function signIn()
    {
        // Check is already authentication
        if (isset($this->auth))
        redirect(base_url("index.php"));

        $this->load->view("portal/sign-in", array(
            "csrf_token" => $this->csrf_token,
            "menu" => $this->menu
        ));
    }

    // Sign Out
    public function signOut()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->session->remove_session(BK_AUTH);
        $this->session->destroy_session();
        redirect(base_url("index.php/views/signIn"));
    }

    // Set Atuh
    public function set_auth()
    {
        $raw = $this->input->post_get("raw", TRUE) ?: "";
        if (!empty($raw)) {
            $auth = json_decode($raw);
            $this->session->add_session(BK_AUTH, $auth);
        } 
        redirect(base_url("index.php"));
    }

    // ------------------------------ DASHBOARD

    // Home
    public function home()
    {
        $this->load->view("home/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'home',
            "menu" => $this->menu
        ));
    }

    public function homeDetail($id)
    {
        $this->load->view("home/detail", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'home',
            "menu" => $this->menu,
            "id_post" => $id
        ));
    }

    // Article
    public function article()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("post/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'article',
            "menu" => $this->menu
        ));
    }

    // Article New
    public function articleNew()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("post/add", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'article',
            "menu" => $this->menu
        ));
    }

    // Article Detail
    public function articleDetail()
    {
        $this->load->view("post/detail", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'article',
            "menu" => $this->menu
        ));
    }

    // Profile
    public function profile()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $target = "";

        switch ($this->auth->type) {
            case "administrator": $target = "profile/detail";
            break;
            case "konseli": $target = "profile/detail-conseli";
            break;
            case "konselor": $target = "profile/detail-conselor";
            break;
        }

        $this->load->view($target, array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'profile',
            "menu" => $this->menu,
            "id" => $this->auth->id
        ));
    }

    // Notification
    public function notification()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("notification/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'notification',
            "menu" => $this->menu
        ));
    }

    // Personal Chat
    public function personalChat()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("chat/personal/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chat',
            "menu" => $this->menu
        ));
    }

    public function personalChatConselor()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("chat/personal/index-conselor", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chat',
            "menu" => $this->menu
        ));
    }

    // Personal Chat Detail
    public function personalChatDetail()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("chat/personal/detail", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chat',
            "menu" => $this->menu
        ));
    }

    // Room Chat
    public function roomChat()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("chat/room/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chat',
            "menu" => $this->menu
        ));
    }

    // Room Chat
    public function roomChatConselor()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("chat/room/index-conselor", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chat',
            "menu" => $this->menu
        ));
    }

    // New Room Chat
    public function roomChatNew()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("chat/room/new", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chat',
            "menu" => $this->menu
        ));
    }

    // Invited Room Chat
    public function roomChatInvitation()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("chat/room/invitation", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chat',
            "menu" => $this->menu
        ));
    }

    // Detail Room Chat
    public function roomChatDetail($id)
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $available = 0;

        switch ($this->auth->type) {
            case "administrator": $available = 1;
            break;
            case "konseli": $available = 0;
            break;
            case "konselor": $available = 1;
            break;
        }

        $this->load->view("chat/room/detail", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chat',
            "menu" => $this->menu,
            "room_id" => $id,
            "available" => $available
        ));
    }

    public function chats()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        if ($this->auth->type != "administrator")
        redirect(base_url("index.php/views/home"));

        $this->load->view("chats/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chats',
            "menu" => $this->menu
        ));
    }

    // Detail Room Chat
    public function roomChatDetailConselor($id)
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        $this->load->view("chat/room/detail-conselor", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'chat',
            "menu" => $this->menu,
            "room_id" => $id
        ));
    }

    // Users
    public function users()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        if ($this->auth->type != "administrator")
        redirect(base_url("index.php/views/home"));

        $this->load->view("users/index", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'users',
            "menu" => $this->menu
        ));
    }

    // Users Detail
    public function usersDetail($id)
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        if ($this->auth->type != "administrator")
        redirect(base_url("index.php/views/home"));

        $this->load->view("users/detail", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'users',
            "menu" => $this->menu,
            "id" => $id
        ));
    }

    // Users Detail
    public function usersDetailConselor($id)
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        if ($this->auth->type != "administrator")
        redirect(base_url("index.php/views/home"));

        $this->load->view("users/detail-conselor", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'users',
            "menu" => $this->menu,
            "id" => $id
        ));
    }

    // Users Detail
    public function usersDetailConseli($id)
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        if ($this->auth->type != "administrator")
        redirect(base_url("index.php/views/home"));

        $this->load->view("users/detail-conseli", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'users',
            "menu" => $this->menu,
            "id" => $id
        ));
    }

    // New Users
    public function usersNew()
    {
        // Check is already authentication
        if (!isset($this->auth))
        redirect(base_url("index.php/views/signIn"));

        if ($this->auth->type != "administrator")
        redirect(base_url("index.php/views/home"));

        $this->load->view("users/new", array(
            "csrf_token" => $this->csrf_token,
            "auth" => $this->auth,
            'routing' => 'users',
            "menu" => $this->menu
        ));
    }

}
