<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sidebar extends CI_Model {
    // Admin
    public function administrator() {
        return array(
            // Dashboard
            array(
                "header" => "Dashboard",
                "items" => array(
                    array(
                        "icon" => "home",
                        "label" => "Beranda",
                        "uri" => base_url("index.php/views/home")
                    )
                )
            ),
            // Pengguna
            array(
                "header" => "Pengguna",
                "items" => array(
                    array(
                        "icon" => "users",
                        "label" => "Kelola Pengguna",
                        "uri" => base_url("index.php/views/users")
                    ),
                    array(
                        "icon" => "chat",
                        "label" => "Kelola Chat",
                        "uri" => base_url("index.php/views/chats")
                    )
                )
            )
        );
    }

    // Konseli
    public function konseli() {
        return array(
            // Dashboard
            array(
                "header" => "Dashboard",
                "items" => array(
                    array(
                        "icon" => "home",
                        "label" => "Beranda",
                        "uri" => base_url("index.php/views/home")
                    ),
                    array(
                        "icon" => "msg-circle",
                        "label" => "Konseling Individu",
                        "uri" => base_url("index.php/views/personalChat")
                    ),
                    array(
                        "icon" => "chat-circle",
                        "label" => "Konseling Kelompok",
                        "uri" => base_url("index.php/views/roomChat")
                    )
                )
            )
        );
    }

    // Konselor
    public function konselor() {
        return array(
            // Dashboard
            array(
                "header" => "Dashboard",
                "items" => array(
                    array(
                        "icon" => "home",
                        "label" => "Beranda",
                        "uri" => base_url("index.php/views/home")
                    ),
                    array(
                        "icon" => "msg-circle",
                        "label" => "Konseling Individu",
                        "uri" => base_url("index.php/views/personalChatConselor")
                    ),
                    array(
                        "icon" => "chat-circle",
                        "label" => "Konseling Kelompok",
                        "uri" => base_url("index.php/views/roomChatConselor")
                    )
                )
            ),
            // Article
            array(
                "header" => "Artikel",
                "items" => array(
                    array(
                        "icon" => "article",
                        "label" => "Artikel Saya",
                        "uri" => base_url("index.php/views/article")
                    ),
                    array(
                        "icon" => "note-add-c",
                        "label" => "Tambah Artikel",
                        "uri" => base_url("index.php/views/articleNew")
                    )
                )
            )
        );
    }

    // Default
    public function default() {
        return array(
            // Dashboard
            array(
                "header" => "Dashboard",
                "items" => array(
                    array(
                        "icon" => "home",
                        "label" => "Beranda",
                        "uri" => base_url("index.php/views/home")
                    ),
                    array(
                        "icon" => "msg-circle",
                        "label" => "Konseling Individu",
                        "uri" => base_url("index.php/views/personalChat")
                    ),
                    array(
                        "icon" => "chat-circle",
                        "label" => "Konseling Kelompok",
                        "uri" => base_url("index.php/views/roomChat")
                    )
                )
            )
        );
    }
}