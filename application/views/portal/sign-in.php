<?php
    $jsDir = base_url() . "assets/js/apps/";

    $this->load->view("templates/base", array(
        "title" => "Portal Masuk - BK SMK Telkom",
        "additional_foot" => '
            <script src="'.$jsDir.'portal/sign-in.js"></script>
        ',
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Portal Masuk</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Sign-In</h2>
                    <div class="nk-block-des">
                        <p>Masukkan user id dan password anda. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <div class="card card-bordered">
            <div class="card-inner">
                <div class="row gy-4">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="user-id">E-Mail/NIK/NIS</label>
                            <input type="text" class="form-control form-control-lg" id="user-id" placeholder="Masukkan E-Mail/NIK/NIS">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-label" for="password">Password</label>
                            <div class="form-control-wrap">
                                <div class="form-icon form-icon-right"
                                onclick="showTogglePassword()">
                                    <em class="icon ni ni-eye"></em>
                                </div>
                                <input type="password" class="form-control form-control-lg" id="password" placeholder="Masukkan Password">
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                            <li>
                                <a href="javascript:void(0)" class="btn btn-lg btn-danger"
                                id="btn-sign-in">Masuk</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        '
    ));
?>