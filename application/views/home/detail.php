<?php
    $jsDir = base_url() . "assets/js/apps/";
    $assetDir = base_url() . "assets/";

    $this->load->view("templates/base", array(
        "title" => "Detail Artikel - BK SMK Telkom",
        "additional_foot" => '
            <script src="'.$jsDir.'home/detail.js"></script>
        ',
        "content" => '

        <input type="hidden" id="id_post" value="'.$id_post.'" />

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Artikel Inspiratif</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal" id="post-title"></h2>
                    <div class="nk-block-des" id="post-meta">
                        
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <div id="detail-post" class="nk-block"></div>
        '
    ));
?>