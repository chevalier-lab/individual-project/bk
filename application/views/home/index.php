<?php
    $jsDir = base_url() . "assets/js/apps/";
    $assetDir = base_url() . "assets/";

    $this->load->view("templates/base", array(
        "title" => "Beranda - BK SMK Telkom",
        "additional_foot" => '
            <script src="'.$jsDir.'home/index.js"></script>
        ',
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Beranda</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Artikel Inspiratif</h2>
                    <div class="nk-block-des">
                        <p>Daftar artikel terbaru untuk anda. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <div id="list-post" class="nk-block"></div>
        '
    ));
?>