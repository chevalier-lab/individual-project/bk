<?php
    $jsDir = base_url() . "assets/js/apps/";
    $assetDir = base_url() . "assets/";

    $this->load->view("templates/base", array(
        "title" => "Buat Artikel - BK SMK Telkom",
        "additional_head" => '
            <link rel="stylesheet" type="text/css" href="'.$assetDir.'summernote/summernote-bs4.css">
        ',
        "additional_foot" => '
            <script type="text/javascript" src="'.$assetDir.'summernote/summernote-bs4.js"></script>
            <script src="'.$jsDir.'post/add.js"></script>
        ',
        "components" => array(
            "post/components/modal-create-category"
        ),
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Kelolah Artikel</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Buat Artikel</h2>
                    <div class="nk-block-des">
                        <p>Buat artikel baru. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <div class="card card-bordered">
            <div class="card-inner">
                <form class="row gy-4" id="form-create-post">
                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="article-title">Judul</label>
                            <input type="text" class="form-control form-control-lg" id="article-title" placeholder="Masukkan Judul Artikel">
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="article-category">Pilih Kategori atau <a href="javascript:void(0)"
                            data-toggle="modal" data-target="#modal-create-category">Buat Baru</a></label>
                            <select id="article-category" class="form-control">
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="article-cover">Sampul</label>
                            <input type="file" class="form-control form-control-lg" id="article-cover">
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12">
                        <div class="form-group">
                            <label class="form-label" for="article-content">Konten</label>
                            <textarea class="form-control form-control-simple no-resize form-control-lg" id="article-content" placeholder="Masukkan Konten Artikel"></textarea>
                        </div>
                    </div>

                    <div class="col-12">
                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                            <li>
                                <a href="javascript:void(0)" class="btn btn-lg btn-danger"
                                id="btn-save">Simpan</a>
                            </li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>

        '
    ));
?>