<?php
    $jsDir = base_url() . "assets/js/apps/";

    $this->load->view("templates/base", array(
        "title" => "Artikel Saya - BK SMK Telkom",
        "additional_foot" => '
            <script src="'.$jsDir.'post/index.js"></script>
        ',
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Kelolah Artikel</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Artikel Saya</h2>
                    <div class="nk-block-des">
                        <p>Daftar artikel inpiratif saya<span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools gx-3">
                        <li class="order-md-last"><a href="'.base_url("index.php/views/articleNew").'" class="btn btn-white btn-dim btn-outline-danger"><span>
                        Buat Artikel
                        </span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <div class="nk-block nk-block-lg">
            <div class="card card-bordered card-preview">
                <div class="card-inner">

                    <div class="table-responsive">
                        <table class="post-list table">
                            <thead>
                                <tr>
                                    <th>Sampul</th>
                                    <th>Judul</th>
                                    <th>Kategori</th>
                                    <th>Tanggal</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div><!-- .card-preview -->
        </div> <!-- nk-block -->

        '
    ));
?>