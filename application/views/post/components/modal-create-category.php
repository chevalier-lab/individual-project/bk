<?php
    $this->load->view("templates/modal", array(
        "modalID" => "modal-create-category",
        "modalStyle" => "modal-md",
        "modalTitle" => "Buat Kategori",
        "modalContent" => '
            <form class="row" id="form-create-category">
                <div class="form-group col-12">
                    <div class="form-group">
                        <label class="form-label" for="modal-create-category-name">Kategori</label>
                        <input type="text" class="form-control" id="modal-create-category-name" placeholder="Masukkan Kategori">
                    </div>
                </div>
            </form>
        ', 
        "modalAction" => '
            <button class="btn btn-danger" type="button"
            id="btn-create-category">
                Simpan
            </button>
        '
    ));
?>