<?php
    $jsDir = base_url() . "assets/js/apps/";

    $this->load->view("templates/base", array(
        "title" => "Kelolah Chat - BK SMK Telkom",
        "additional_foot" => '
            <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
            <script src="'.$jsDir.'chats/index.js"></script>
        ',
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Kelolah Chat</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Daftar Chat Personal</h2>
                    <div class="nk-block-des">
                        <p>Daftar Chat yang terdapat di sistem BK SMK TELKOM MAKASSAR. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <div class="nk-block nk-block-lg">
            <div class="card card-bordered card-preview">
                <div class="card-inner">

                    <div class="table-responsive">
                        <table class="chat-list table">
                            <thead>
                                <tr>
                                    <th>Aksi</th>
                                    <th>Token</th>
                                    <th>Nama Konseli</th>
                                    <th>Nama Konselor</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div><!-- .card-preview -->
        </div> <!-- nk-block -->

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Kelolah Chat</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Daftar Chat Group</h2>
                    <div class="nk-block-des">
                        <p>Daftar Chat Group yang terdapat di sistem BK SMK TELKOM MAKASSAR. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <div class="nk-block nk-block-lg">
            <div class="card card-bordered card-preview">
                <div class="card-inner">

                    <div class="table-responsive">
                        <table class="chat-group-list table">
                            <thead>
                                <tr>
                                    <th>Aksi</th>
                                    <th>Token</th>
                                    <th>Nama Konselor</th>
                                    <th>Judul</th>
                                    <th>Tanggal</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div><!-- .card-preview -->
        </div> <!-- nk-block -->

        '
    ));
?>