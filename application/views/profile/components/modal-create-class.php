<?php
    $this->load->view("templates/modal", array(
        "modalID" => "modal-create-class",
        "modalStyle" => "modal-md",
        "modalTitle" => "Buat Kelas",
        "modalContent" => '
            <form class="row" id="form-create-class">
                <div class="form-group col-12">
                    <div class="form-group">
                        <label class="form-label" for="modal-create-class-class_name">Nama Kelas</label>
                        <input type="text" class="form-control" id="modal-create-class-class_name" placeholder="Masukkan Nama Kelas">
                    </div>
                </div>
                <div class="col-12">
                    <div class="form-group">
                        <label class="form-label" for="modal-create-class-class_code">Kode Kelas</label>
                        <input type="text" class="form-control" id="modal-create-class-class_code" placeholder="Masukkan Kode Kelas">
                    </div>
                </div>
            </form>
        ', 
        "modalAction" => '
            <button class="btn btn-danger" type="button"
            id="btn-create-class">
                Simpan
            </button>
        '
    ));
?>