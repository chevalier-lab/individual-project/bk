<?php
    $this->load->view("templates/modal", array(
        "modalID" => "modal-change-photo",
        "modalStyle" => "modal-sm",
        "modalTitle" => "Ganti Foto Profil",
        "modalContent" => '
            <form class="row" id="form-change-photo">
                <div class="col-12">
                    <center>
                    <img src="" id="modal-change-photo-preview"
                    stylex="max-width: 120px;" />
                    </center>
                </div>
                <div class="form-group col-12">
                    <div class="form-group">
                        <label class="form-label" for="modal-change-photo-input">Foto Profil Baru</label>
                        <input type="file" class="form-control" id="modal-change-photo-input">
                    </div>
                </div>
            </form>
        ', 
        "modalAction" => '
            <button class="btn btn-danger" type="button"
            id="btn-modal-change-photo">
                Simpan
            </button>
        '
    ));
?>