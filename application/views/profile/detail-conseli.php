<?php
    $jsDir = base_url() . "assets/js/apps/";

    $this->load->view("templates/base", array(
        "title" => "Profil Pengguna - BK SMK Telkom",
        "additional_foot" => '
            <script src="'.$jsDir.'users/detail-conseli.js"></script>
        ',
        "components" => array(
            "users/components/modal-create-class",
            "profile/components/modal-change-password-user",
            "profile/components/modal-change-photo"
        ),
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Kelolah Pengguna</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Profil Pengguna</h2>
                    <div class="nk-block-des">
                        <p>Profil Pengguna. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools gx-3">
                        <li class="order-md-last"><a href="javascript:void(0);" class="btn btn-white btn-dim btn-outline-danger"
                        data-toggle="modal" data-target="#modal-change-password-user"><span>
                        Ganti Password
                        </span></a></li>
                        <li class="order-md-last"><a href="javascript:void(0);" class="btn btn-white btn-dim btn-outline-success"
                        data-toggle="modal" data-target="#modal-change-photo"><span>
                        Ganti Foto
                        </span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <input type="hidden" id="id_m_users" value="'.$id.'">

        <div class="card card-bordered">
            <div class="card-inner">
                <form class="gy-4" id="form-create-user">
                    <div class="row">
                        <div class="col-sm-12">
                            <strong>Biodata Pengguna</strong>
                        </div>
                    </div>

                    <div class="row" id="container-biodata">
                        
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="full_name">Nama Lengkap</label>
                                <input type="text" class="form-control" id="full_name" placeholder="Masukkan Nama Lengkap">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="call_name">Nama Panggilan</label>
                                <input type="text" class="form-control" id="call_name" placeholder="Masukkan Nama Panggilan">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="gender">Jenis Kelamin</label>
                                <select id="gender" class="form-control">
                                    <option value="" selected>Pilih Jenis Kelamin</option>
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita">Wanita</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="religion">Agama</label>
                                <select id="religion" class="form-control">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen Protestan">Kristen Protestan</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Buddha">Buddha</option>
                                    <option value="Kong Hu Cu">Kong Hu Cu</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="phone_number">Nomor Telepon</label>
                                <input type="number" class="form-control" id="phone_number" placeholder="Masukkan Nomor Telepon">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="address">Alamat</label>
                                <input type="text" class="form-control" id="address" placeholder="Masukkan Alamat">
                            </div>
                        </div>

                    </div>

                    <div id="container-biodata-divider">
                    <hr>
                    </div>

                    <div class="row" id="container-additional-label">
                        <div class="col-sm-12">
                            <strong>Informasi Tambahan</strong>
                        </div>
                    </div>

                    <div class="row" id="container-additional">

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="place_of_birth">Tempat Lahir</label>
                                <input type="text" class="form-control" id="place_of_birth" placeholder="Masukkan Tempat Lahir">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="date_of_birth">Tanggal Lahir</label>
                                <input type="date" class="form-control" id="date_of_birth" placeholder="Masukkan Tanggal Lahir">
                            </div>
                        </div>

                    </div>

                    <div id="container-additional-divider">
                    <hr>
                    </div>

                    <div class="row" id="container-student-label">
                        <div class="col-sm-12">
                            <strong>Informasi Siswa</strong>
                        </div>
                    </div>

                    <div class="row" id="container-student">

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="class_name">Pilih Kelas atau <a href="javascript:void(0)"
                                data-toggle="modal" data-target="#modal-create-class">Buat Baru</a></label>
                                <select id="class_name" class="form-control">
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="nis">NIS</label>
                                <input type="text" class="form-control" id="nis" placeholder="Masukkan NIS">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="living_together">Tinggal Bersama</label>
                                <select id="living_together" class="form-control">
                                    <option value="Orang Tua">Orang Tua</option>
                                    <option value="Wali">Wali</option>
                                    <option value="Kos">Kos</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="distance_from_home">Jarak Rumah Ke Sekolah</label>
                                <div class="form-control-wrap">
                                    <div class="form-icon form-icon-right">
                                        Km
                                    </div>
                                    <input type="number" class="form-control" placeholder="Jarak Rumah Ke Sekolah"
                                    id="distance_from_home">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="vehicle">Berangkat Ke Sekolah Dengan</label>
                                <select id="vehicle" class="form-control">
                                    <option value="Jalan Kaki">Jalan Kaki</option>
                                    <option value="Kendaraan Umum">Kendaraan Umum</option>
                                    <option value="Kendaraan Pribadi">Kendaraan Pribadi</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="child_position">Anak Ke</label>
                                <input type="number" class="form-control" id="child_position" placeholder="Anak Ke">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="brother_size">Jumlah Bersaudara</label>
                                <input type="number" class="form-control" id="brother_size" placeholder="Jumlah Bersaudara">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="brother_male_size">Jumlah Kakak Laki-Laki</label>
                                <input type="number" class="form-control" id="brother_male_size" placeholder="Jumlah Kakak Laki-Laki">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="brother_female_size">Jumlah Kakak Perempuan</label>
                                <input type="number" class="form-control" id="brother_female_size" placeholder="Jumlah Kakak Perempuan">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="litle_brother_male_size">Jumlah Adik Laki-Laki</label>
                                <input type="number" class="form-control" id="litle_brother_male_size" placeholder="Jumlah Adik Laki-Laki">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="litle_brother_female_size">Jumlah Adik Perempuan</label>
                                <input type="number" class="form-control" id="litle_brother_female_size" placeholder="Jumlah Adik Perempuan">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="hobby">Hobi</label>
                                <input type="text" class="form-control" id="hobby" placeholder="Hobi">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="history_of_disease">Riwayat Penyakit</label>
                                <input type="text" class="form-control" id="history_of_disease" placeholder="Riwayat Penyakit">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="interest">Minat / Bakat</label>
                                <input type="text" class="form-control" id="interest" placeholder="Minat / Bakat">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="problem">Masalah Berat Yang Pernah Dialami</label>
                                <input type="text" class="form-control" id="problem" placeholder="Masalah Berat Yang Pernah Dialami">
                            </div>
                        </div>

                    </div>

                    <div id="container-student-divider">
                    <hr>
                    </div>

                    <div class="row" id="container-student-father-label">
                        <div class="col-sm-12">
                            <strong>Informasi Ayah Siswa</strong>
                        </div>
                    </div>

                    <div class="row" id="container-student-father">

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="name_father">Nama Ayah</label>
                                <input type="text" class="form-control" id="name_father" placeholder="Nama Ayah">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="religion_father">Agama Ayah</label>
                                <select id="religion_father" class="form-control">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen Protestan">Kristen Protestan</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Buddha">Buddha</option>
                                    <option value="Kong Hu Cu">Kong Hu Cu</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="place_of_birth_father">Tempat Lahir Ayah</label>
                                <input type="text" class="form-control" id="place_of_birth_father" placeholder="Tempat Lahir Ayah">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="date_of_birth_father">Tanggal Lahir Ayah</label>
                                <input type="date" class="form-control" id="date_of_birth_father" placeholder="Tanggal Lahir Ayah">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="job_father">Pekerjaan Ayah</label>
                                <input type="text" class="form-control" id="job_father" placeholder="Pekerjaan Ayah">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="address_father">Alamat Ayah</label>
                                <input type="text" class="form-control" id="address_father" placeholder="Alamat Ayah">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="phone_number_father">Nomor HP Ayah</label>
                                <input type="text" class="form-control" id="phone_number_father" placeholder="Nomor HP Ayah">
                            </div>
                        </div>

                    </div>

                    <div id="container-student-father-divider">
                    <hr>
                    </div>

                    <div class="row" id="container-student-mother-label">
                        <div class="col-sm-12">
                            <strong>Informasi Ibu Siswa</strong>
                        </div>
                    </div>

                    <div class="row" id="container-student-mother">

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="name_mother">Nama Ibu</label>
                                <input type="text" class="form-control" id="name_mother" placeholder="Nama Ibu">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="religion_mother">Agama Ibu</label>
                                <select id="religion_mother" class="form-control">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen Protestan">Kristen Protestan</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Buddha">Buddha</option>
                                    <option value="Kong Hu Cu">Kong Hu Cu</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="place_of_birth_mother">Tempat Lahir Ibu</label>
                                <input type="text" class="form-control" id="place_of_birth_mother" placeholder="Tempat Lahir Ibu">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="date_of_birth_mother">Tanggal Lahir Ibu</label>
                                <input type="date" class="form-control" id="date_of_birth_mother" placeholder="Tanggal Lahir Ibu">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="job_mother">Pekerjaan Ibu</label>
                                <input type="text" class="form-control" id="job_mother" placeholder="Pekerjaan Ibu">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="address_mother">Alamat Ibu</label>
                                <input type="text" class="form-control" id="address_mother" placeholder="Alamat Ibu">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="phone_number_mother">Nomor HP Ibu</label>
                                <input type="text" class="form-control" id="phone_number_mother" placeholder="Nomor HP Ibu">
                            </div>
                        </div>

                    </div>

                    <div id="container-student-mother-divider">
                    <hr>
                    </div>

                    <div class="row" id="container-student-wali-label">
                        <div class="col-sm-12">
                            <strong>Informasi Wali Siswa</strong>
                        </div>
                    </div>

                    <div class="row" id="container-student-wali">

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="name_wali">Nama Wali</label>
                                <input type="text" class="form-control" id="name_wali" placeholder="Nama Wali">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="religion_wali">Agama Wali</label>
                                <select id="religion_wali" class="form-control">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen Protestan">Kristen Protestan</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Buddha">Buddha</option>
                                    <option value="Kong Hu Cu">Kong Hu Cu</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="place_of_birth_wali">Tempat Lahir Wali</label>
                                <input type="text" class="form-control" id="place_of_birth_wali" placeholder="Tempat Lahir Wali">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="date_of_birth_wali">Tanggal Lahir Wali</label>
                                <input type="date" class="form-control" id="date_of_birth_wali" placeholder="Tanggal Lahir Wali">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="job_wali">Pekerjaan Wali</label>
                                <input type="text" class="form-control" id="job_wali" placeholder="Pekerjaan Wali">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="address_wali">Alamat Wali</label>
                                <input type="text" class="form-control" id="address_wali" placeholder="Alamat Wali">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="phone_number_wali">Nomor HP Wali</label>
                                <input type="text" class="form-control" id="phone_number_wali" placeholder="Nomor HP Wali">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                <li>
                                    <a href="javascript:void(0)" class="btn btn-lg btn-danger"
                                    id="btn-save">Simpan</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        '
    ));
?>