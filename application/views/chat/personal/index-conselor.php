<?php
    $jsDir = base_url() . "assets/js/apps/";
    $assetDir = base_url() . "assets/";

    $this->load->view("templates/base", array(
        "title" => "Konseling Individual - BK SMK Telkom",
        "additional_foot" => '
            <script src="'.$jsDir.'chat/personal/index-consoler.js"></script>
        ',
        "components" => array(
            // "chat/personal/components/modal-create-room",
            "chat/personal/components/modal-create-room-group"
        ),
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Konseling</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Konseling Individual</h2>
                    <div class="nk-block-des">
                        <p>Daftar konseling individu anda. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <div class="card card-bordered">
            <div class="card-inner p-0 m-0">
            
                <div class="nk-msg">
                    <div class="nk-msg-aside">
                        <div class="nk-msg-list" data-simplebar id="list-room">
                        </div><!-- .nk-msg-list -->
                    </div><!-- .nk-msg-aside -->
                    <div class="nk-msg-body bg-white profile-shown">
                        <div class="nk-msg-head">
                            <h4 class="title d-none d-lg-block">Konseling Individual</h4>
                            <div class="nk-msg-head-meta">
                                <div class="d-none d-lg-block">
                                    <ul class="nk-msg-tags">
                                        <li><span class="label-tag"><em class="icon ni ni-flag-fill"></em> <span id="chat-partner-name"></span></span></li>
                                    </ul>
                                </div>
                                <div class="d-lg-none"><a href="#" class="btn btn-icon btn-trigger nk-msg-hide ml-n1"><em class="icon ni ni-arrow-left"></em></a></div>
                                <ul class="nk-msg-actions">
                                    <li><a href="javascript:void(0)" 
                                    data-toggle="modal" data-target="#modal-create-room-group"
                                    class="btn btn-dim btn-sm btn-outline-light"><em class="icon ni ni-check"></em><span>Alihkan ke Konseling Kelompok</span></a></li>
                                </ul>
                            </div>
                        </div><!-- .nk-msg-head -->
                        <div class="nk-msg-reply nk-reply" data-simplebar id="list-chat">
                        </div><!-- .nk-reply -->


                        <div class="nk-reply-form m-2">
                            <div class="tab-content">
                                <div class="tab-pane active" id="reply-form">
                                    <div class="nk-reply-form-editor">
                                        <div class="nk-reply-form-field">
                                            <textarea class="form-control form-control-simple no-resize" placeholder="Hello"
                                            id="chat-content"></textarea>
                                        </div>
                                        <div class="nk-reply-form-tools">
                                            <ul class="nk-reply-form-actions g-1">
                                                <li class="mr-2"><button class="btn btn-danger" type="button" id="btn-send-chat">Kirim</button></li>
                                                <li>
                                                    <label for="chat-document" class="btn btn-icon btn-sm" data-toggle="tooltip" data-placement="top" title="Upload Attachment" href="javascript:void(0);">
                                                        <em class="icon ni ni-clip-v"></em>
                                                        <input id="chat-document" type="file" style="display: none;"
                                                        onchange="sendChatDocument(this)"/>
                                                    </label>
                                                </li>
                                                <li>
                                                    <label for="chat-photo" class="btn btn-icon btn-sm" data-toggle="tooltip" data-placement="top" title="Upload Images" href="javascript:void(0);">
                                                        <em class="icon ni ni-img"></em>
                                                        <input id="chat-photo" type="file" style="display: none;"
                                                        onchange="sendChatPhoto(this)"/>
                                                    </label>
                                                </li>
                                            </ul>
                                        </div><!-- .nk-reply-form-tools -->
                                    </div><!-- .nk-reply-form-editor -->
                                </div>
                            </div>
                        </div><!-- .nk-reply-form -->
                    </div><!-- .nk-msg-body -->
                </div><!-- .nk-msg -->

            </div>
        </div>

        '
    ));
?>