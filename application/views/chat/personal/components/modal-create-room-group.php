<?php
    $this->load->view("templates/modal", array(
        "modalID" => "modal-create-room-group",
        "modalStyle" => "modal-lg",
        "modalTitle" => "Buat Konseling Kelompok",
        "modalContent" => '
            <div id="container-create-room-group">
                <h4><center>Tahapan Konseling Kelompok</center></h4>
                <div class="py-1">
                <strong>A. Tahap Pembentukan</strong>
                </div>
                <ol>
                    <li>1. Menerima Secara terbuka dan mengucapkan terimakasih</li>
                    <li>2. Berdoa</li>
                    <li>3. Menjelaskan Pengertian Konseling Kelompok</li>
                    <li>4. Menjelaskan Tujuan Konseling Kelompok</li>
                    <li>5. Menjelaskan Cara Pelaksanaan Konseling Kelompok</li>
                    <li>6. Menjelaskan Asas-Asas Konseling Kelompok</li>
                    <li>7. Melaksanakan Perkenalan dilanjutkan dengan Rangkaian Nama atau permainan yang relevan</li>
                </ol>
                <div class="py-1">
                <strong>B. Tahap Peralihan</strong>
                </div>
                <ol>
                    <li>1. Menjelaskan Kembali Kegiatan Konseling Kelompok</li>
                    <li>2. Tanya Jawab tentang Kesiapan Anggota untuk Kegiatan Lebih Lanjut </li>
                    <li>3. Mengenali Suasana Apabila Anggota secara Keseluruhan/Sebagian Belum Siap untuk Memasuki Tahap Berikutnya dan Mengatasi Suasana Tersebut</li>
                    <li>4. Memberi Contoh Masalah Pribadi yang dapat Dikemukakan dan Dibahas dalam Kelompok</li>
                </ol>
                <div class="py-1">
                <strong>C. Tahap Kegiatan</strong>
                </div>
                <ol>
                    <li>1. Menjelaskan Masalah Pribadi yang Hendaknya Dikemukakan oleh Anggota Kelompok</li>
                    <li>2. Mempersilahkan Anggota untuk Mengemukakan Masalah Pribadi Masing-Masing Secara Bergantian</li>
                    <li>3. Menetapkan Masalah yang akan Dibahas</li>
                    <li>4. Membahas Masalah Terpilih Secara Tuntas </li>
                    <li>5. Selingan/Permainan yang Relevan</li>
                    <li>6. Menegaskan Komitmen Anggota yang Masalahnya Telah Dibahas ( Apa yang akan dilakukan Berkenaan adanya  Pembahasan Demi Penyelesaian Masalah)</li>
                </ol>
                <div class="py-1">
                <strong>D. Tahap Pengakhiran</strong>
                </div>
                <ol>
                    <li>1. Menjelaskan Bahwa Kegiatan Konseling Kelompok akan diakhiri </li>
                    <li>2. Anggota Memberikan Kesan dan Menilai Kemajuan yang Akan Dicapai Masing-Masing</li>
                    <li>3. Pembahasan Kegiatan Lanjutan</li>
                    <li>4. Pesan Serta Tanggapan Anggota</li>
                    <li>5. Mengucapkan Terimakasih</li>
                    <li>6. Berdoa</li>
                    <li>7. Perpisahan</li>
                </ol>
            </div>
            <form class="row" id="form-create-room-group">

                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="form-label" for="room-group-title">Judul Konseling</label>
                        <input type="text" class="form-control form-control-lg" id="room-group-title" placeholder="Masukkan Judul Konseling Kelompok">
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="form-label" for="room-group-description">Deksripsi</label>
                        <textarea class="form-control form-control-lg" id="room-group-description" placeholder="Masukkan Deksripsi"></textarea>
                    </div>
                </div>

            </form>
        ', 
        "modalAction" => '
        <button class="btn btn-lg btn-danger"
        id="btn-save-room-group">Simpan</a>
        '
    ));
?>
