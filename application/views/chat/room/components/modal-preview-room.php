<?php
    $this->load->view("templates/modal", array(
        "modalID" => "modal-preview-room",
        "modalStyle" => "modal-lg",
        "modalTitle" => "Detail Konseling Kelompok",
        "modalContent" => '
            <div class="row" id="preview-room"></div>
        ', 
        "modalAction" => '
        '
    ));
?>