<?php
    $this->load->view("templates/modal", array(
        "modalID" => "modal-invite-member",
        "modalStyle" => "modal-md",
        "modalTitle" => "Undang Siswa",
        "modalContent" => '
            <form class="row" id="invite-member">
                
                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="form-label" for="invite-member-class">Pilih Kelas</label>
                        <select class="form-control form-control-lg" id="invite-member-class"
                        onchange="loadSiswa(this.value)">
                            <option value="" disabled>Pilih Kelas</option>
                        </select>
                    </div>
                </div>

                <div class="col-sm-12">
                    <div class="form-group">
                        <label class="form-label" for="invite-member-student">Pilih Siswa</label>
                        <select class="form-control form-control-lg" id="invite-member-student">
                            <option value="">Pilih Siswa</option>
                        </select>
                    </div>
                </div>

            </form>
        ', 
        "modalAction" => '
        <a href="javascript:void(0)" class="btn btn-lg btn-danger"
        id="btn-save-invite-member">Undang</a>
        '
    ));
?>