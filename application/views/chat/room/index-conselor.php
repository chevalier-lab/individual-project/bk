<?php
    $jsDir = base_url() . "assets/js/apps/";
    $assetDir = base_url() . "assets/";

    $this->load->view("templates/base", array(
        "title" => "Konseling Kelompok - BK SMK Telkom",
        "additional_foot" => '
            <script src="'.$jsDir.'chat/room/index-conselor.js"></script>
        ',
        "components" => array(
            "chat/room/components/modal-preview-room",
        ),
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Konseling</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Konseling Kelompok</h2>
                    <div class="nk-block-des">
                        <p>Daftar konseling kelompok anda. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <section id="list-room" class="row"></section>

        '
    ));
?>