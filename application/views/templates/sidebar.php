<div class="nk-aside" data-content="sideNav" data-toggle-overlay="true" data-toggle-screen="lg" data-toggle-body="true">
    <div class="nk-sidebar-menu" data-simplebar>
        <!-- Menu -->
        <ul class="nk-menu">

        <?php
            if (isset($menu)) {
                foreach ($menu as $itemMenu) {
                    ?>
                    <li class="nk-menu-heading">
                        <h6 class="overline-title"><?= $itemMenu["header"]; ?></h6>
                    </li>
                    <?php
                    foreach ($itemMenu["items"] as $item) {
                        ?>
                        <li class="nk-menu-item">
                            <a href="<?= $item['uri'] ?>" class="nk-menu-link">
                                <span class="nk-menu-icon"><em class="icon ni ni-<?= $item['icon']; ?>"></em></span>
                                <span class="nk-menu-text"><?= $item['label']; ?></span>
                            </a>
                        </li>
                        <?php
                    }
                }
            }
        ?>
          
        </ul>
    </div><!-- .nk-sidebar-menu -->
    <div class="nk-aside-close">
        <a href="#" class="toggle" data-target="sideNav"><em class="icon ni ni-cross"></em></a>
    </div><!-- .nk-aside-close -->
</div><!-- .nk-aside -->