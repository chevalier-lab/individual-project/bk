<!-- Modal -->
<div class="modal fade" id="<?= isset($modalID) ? $modalID : ''; ?>" 
tabindex="-1" role="dialog" aria-labelledby="<?= isset($modalID) ? $modalID : ''; ?>Title" aria-hidden="true">
  <div class="modal-dialog <?= isset($modalStyle) ? $modalStyle : 'modal-md'; ?>" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="<?= isset($modalID) ? $modalID : ''; ?>Title">
            <?= isset($modalTitle) ? $modalTitle : ''; ?>
        </h5>
      </div>
      <div class="modal-body">
        <?= isset($modalContent) ? $modalContent : ''; ?>
      </div>
      <?php
        if (isset($modalAction)) {
            ?>
            <div class="modal-footer">
                <?= $modalAction; ?>
            </div>
            <?php
        }
      ?>
    </div>
  </div>
</div>