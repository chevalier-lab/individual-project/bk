    <!-- JavaScript -->
    <script src="<?= base_url() . 'assets/js/bundle.js?ver=1.4.0'; ?>"></script>
    <script src="<?= base_url() . 'assets/js/scripts.js?ver=1.4.0'; ?>"></script>

    <script src="<?= base_url() . 'assets/js/apps/general.js'; ?>"></script>

    <?php
        if (isset($auth)) {
            ?>
            <script src="<?= base_url() . 'assets/js/apps/global-auth.js'; ?>"></script>
            <?php
        }
        if (isset($additional_foot)) echo $additional_foot;
    ?>
</body>

</html>