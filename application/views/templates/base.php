<?php
    // Load Head
    $this->load->view("templates/head");
?>

<input type="hidden" value="<?= base_url("index.php"); ?>" id="base_url">
<input type="hidden" value="<?= $csrf_token; ?>" id="csrf_token">

<div class="nk-app-root">
    <!-- main @s -->
    <div class="nk-main ">
        <!-- wrap @s -->
        <div class="nk-wrap ">
            <?php 
                // Load Top Bar
                $this->load->view("templates/topbar"); 
            ?>
            <!-- content @s -->
            <div class="nk-content ">
                <div class="container wide-xl">
                    <div class="nk-content-inner">
                        <?php 
                            // Load Sidebar
                            $this->load->view("templates/sidebar"); 
                        ?>
                        <div class="nk-content-body">
                            <div class="nk-content-wrap">
                                <?php
                                    if (isset($content)) echo $content;
                                ?>
                            </div>
                            <!-- footer @s -->
                            <div class="nk-footer">
                                <div class="container wide-xl">
                                    <div class="nk-footer-wrap g-2">
                                        <div class="nk-footer-copyright"> &copy; 2021 BK SMK TELKOM MAKASSAR. | Powered by <a href="#">Chevalier Lab</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- footer @e -->
                        </div>
                    </div>
                </div>
            </div>
            <!-- content @e -->
        </div>
        <!-- wrap @e -->
    </div>
    <!-- main @e -->
</div>
<!-- app-root @e -->

<?php
    // Load Component
    if (isset($components)) {
        foreach ($components as $item) {
            $this->load->view($item);
        }
    }

    // Load Foot
    $this->load->view("templates/foot");
?>