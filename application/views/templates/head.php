<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="@@page-discription">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title><?= isset($title) ? $title : ''; ?></title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="<?= base_url() . 'assets/css/dashlite.css?ver=1.4.0'; ?>">
    <link id="skin-default" rel="stylesheet" href="<?= base_url() . 'assets/css/skins/theme-red.css?ver=1.4.0'; ?>">

    <?php
        if (isset($additional_head)) echo $additional_head;
    ?>
</head>
<body class="nk-body npc-subscription has-aside ui-clean ">