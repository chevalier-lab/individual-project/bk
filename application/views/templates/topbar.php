
<div class="nk-header nk-header-fixed is-light">
    <div class="container-lg wide-xl">
        <div class="nk-header-wrap">
            <div class="nk-header-brand">
                <a href="<?= base_url(); ?>" class="logo-link">
                    <img class="logo-light logo-img" src="<?= base_url() . 'images/logo-fix.png'; ?>" srcset="<?= base_url() . 'images/logo-fix.png 2x'; ?>" alt="logo">
                    <img class="logo-dark logo-img" src="<?= base_url() . 'images/logo-fix.png'; ?>" srcset="<?= base_url() . 'images/logo-fix.png 2x'; ?>" alt="logo-dark">
                </a>
            </div><!-- .nk-header-brand -->
            <div class="nk-header-tools">
                <ul class="nk-quick-nav">
                    <?php
                        if (isset($auth)) {
                            ?>
                            <li class="dropdown user-dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <div class="user-toggle" id="topbar-profile">
                                        <div class="user-avatar sm">
                                            <em class="icon ni ni-user-alt"></em>
                                        </div>
                                        <div class="user-name dropdown-indicator d-none d-sm-block">Abu Bin Ishityak</div>
                                    </div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                    <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                        <div class="user-card" id="topbar-card-profile">
                                            <div class="user-avatar">
                                                <span>AB</span>
                                            </div>
                                            <div class="user-info">
                                                <span class="lead-text">Abu Bin Ishtiyak</span>
                                                <span class="sub-text">info@softnio.com</span>
                                            </div>
                                            <div class="user-action">
                                                <a class="btn btn-icon mr-n2" href="<?= base_url("index.php/views/profile") ?>"><em class="icon ni ni-setting"></em></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dropdown-inner">
                                        <ul class="link-list">
                                            <li><a href="<?= base_url("index.php/views/profile") ?>"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                        </ul>
                                    </div>
                                    <div class="dropdown-inner">
                                        <ul class="link-list">
                                            <li><a href="<?= base_url('index.php/views/signOut'); ?>"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                        </ul>
                                    </div>
                                </div>
                            </li><!-- .dropdown -->
                            <li class="dropdown notification-dropdown">
                                <a href="#" class="dropdown-toggle nk-quick-nav-icon mr-lg-n1" data-toggle="dropdown">
                                    <div class="icon-status icon-status-info"><em class="icon ni ni-bell"></em></div>
                                </a>
                                <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right dropdown-menu-s1">
                                    <div class="dropdown-head">
                                        <span class="sub-title nk-dropdown-title">Notifications</span>
                                    </div>
                                    <div class="dropdown-body">
                                        <div class="nk-notification" id="notification-list">
                                            
                                        </div><!-- .nk-notification -->
                                    </div>
                                </div>
                            </li><!-- .dropdown -->
                            <?php
                        } else {
                            ?>
                            <li>
                                <a href="<?= base_url("index.php/views/signIn") ?>">
                                    <div class="user-toggle">
                                        <div class="user-avatar sm">
                                            <em class="icon ni ni-user-alt"></em>
                                        </div>
                                        <div class="user-name d-none d-sm-block">Masuk</div>
                                    </div>
                                </a>
                            </li>
                            <?php
                        }
                    ?>
                    <li class="d-lg-none">
                        <a href="#" class="toggle nk-quick-nav-icon mr-n1" data-target="sideNav"><em class="icon ni ni-menu"></em></a>
                    </li>
                </ul><!-- .nk-quick-nav -->
            </div><!-- .nk-header-tools -->
        </div><!-- .nk-header-wrap -->
    </div><!-- .container-fliud -->
</div>
<!-- main header @e -->