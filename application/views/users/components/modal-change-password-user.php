<?php
    $this->load->view("templates/modal", array(
        "modalID" => "modal-change-password-user",
        "modalStyle" => "modal-md",
        "modalTitle" => "Ganti Password",
        "modalContent" => '
            <form class="row" id="form-change-password-user">
                <div class="form-group col-12">
                    <div class="form-group">
                        <label class="form-label" for="modal-change-password-new">Password Baru</label>
                        <input type="password" class="form-control" id="modal-change-password-new" placeholder="Masukkan Password Baru">
                    </div>
                </div>
            </form>
        ', 
        "modalAction" => '
            <button class="btn btn-danger" type="button"
            id="btn-change-password-user">
                Simpan
            </button>
        '
    ));
?>