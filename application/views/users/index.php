<?php
    $jsDir = base_url() . "assets/js/apps/";

    $this->load->view("templates/base", array(
        "title" => "Kelolah Pengguna - BK SMK Telkom",
        "additional_foot" => '
            <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
            <script src="'.$jsDir.'users/index.js"></script>
        ',
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Kelolah Pengguna</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Daftar Pengguna</h2>
                    <div class="nk-block-des">
                        <p>Daftar pengguna yang terdapat di sistem BK SMK TELKOM MAKASSAR. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools gx-3">
                        <li class="order-md-last"><a href="'.base_url("index.php/views/usersNew").'" class="btn btn-white btn-dim btn-outline-danger"><span>
                        Buat Pengguna
                        </span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <div class="nk-block nk-block-lg">
            <div class="card card-bordered card-preview">
                <div class="card-inner">

                    <div class="table-responsive">
                        <table class="user-list table">
                            <thead>
                                <tr>
                                    <th>Aksi</th>
                                    <th>Nama Lengkap</th>
                                    <th>Telepon</th>
                                    <th>Jenis Kelamin</th>
                                    <th>Agama</th>
                                    <th>Jenis Akun</th>
                                    <th>Tgl Buat</th>
                                    <th>Tgl Ubah</th>
                                    <th>Detail</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                    
                </div>
            </div><!-- .card-preview -->
        </div> <!-- nk-block -->

        '
    ));
?>