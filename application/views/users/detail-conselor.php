<?php
    $jsDir = base_url() . "assets/js/apps/";

    $this->load->view("templates/base", array(
        "title" => "Detail Pengguna - BK SMK Telkom",
        "additional_foot" => '
            <script src="'.$jsDir.'users/detail-conselor.js"></script>
        ',
        "components" => array(
            "users/components/modal-create-class",
            "users/components/modal-change-password-user"
        ),
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Kelolah Pengguna</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Detail Pengguna</h2>
                    <div class="nk-block-des">
                        <p>Detail pengguna. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools gx-3">
                        <li class="order-md-last"><a href="javascript:void(0);" class="btn btn-white btn-dim btn-outline-danger"
                        data-toggle="modal" data-target="#modal-change-password-user"><span>
                        Ganti Password
                        </span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <input type="hidden" id="id_m_users" value="'.$id.'">

        <div class="card card-bordered">
            <div class="card-inner">
                <form class="gy-4" id="form-save-user">
                    <div class="row">
                        <div class="col-sm-12">
                            <strong>Biodata Pengguna</strong>
                        </div>
                    </div>

                    <div class="row" id="container-biodata">
                        
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="full_name">Nama Lengkap</label>
                                <input type="text" class="form-control" id="full_name" placeholder="Masukkan Nama Lengkap">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="call_name">Nama Panggilan</label>
                                <input type="text" class="form-control" id="call_name" placeholder="Masukkan Nama Panggilan">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="gender">Jenis Kelamin</label>
                                <select id="gender" class="form-control">
                                    <option value="" selected>Pilih Jenis Kelamin</option>
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita">Wanita</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="religion">Agama</label>
                                <select id="religion" class="form-control">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen Protestan">Kristen Protestan</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Buddha">Buddha</option>
                                    <option value="Kong Hu Cu">Kong Hu Cu</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="phone_number">Nomor Telepon</label>
                                <input type="number" class="form-control" id="phone_number" placeholder="Masukkan Nomor Telepon">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="address">Alamat</label>
                                <input type="text" class="form-control" id="address" placeholder="Masukkan Alamat">
                            </div>
                        </div>

                    </div>

                    <div id="container-biodata-divider">
                    <hr>
                    </div>

                    <div class="row" id="container-additional-label">
                        <div class="col-sm-12">
                            <strong>Informasi Tambahan</strong>
                        </div>
                    </div>

                    <div class="row" id="container-additional">

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="place_of_birth">Tempat Lahir</label>
                                <input type="text" class="form-control" id="place_of_birth" placeholder="Masukkan Tempat Lahir">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="date_of_birth">Tanggal Lahir</label>
                                <input type="date" class="form-control" id="date_of_birth" placeholder="Masukkan Tanggal Lahir">
                            </div>
                        </div>

                    </div>

                    <div id="container-additional-divider">
                    <hr>
                    </div>

                    <div class="row" id="container-conselor-label">
                        <div class="col-sm-12">
                            <strong>Informasi Konselor</strong>
                        </div>
                    </div>

                    <div class="row" id="container-conselor">

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="marital_status_conselor">Status Perkawinan</label>
                                <input type="text" class="form-control" id="marital_status_conselor" placeholder="Status Perkawinan">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="last_education_conselor">Pendidikan Terakhir</label>
                                <select id="last_education_conselor" class="form-control">
                                    <option value="D3">D3</option>
                                    <option value="S1">S1</option>
                                    <option value="S2">S2</option>
                                    <option value="S3">S3</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="school_name_conselor">Nama Sekolah</label>
                                <input type="text" class="form-control" id="school_name_conselor" placeholder="Nama Sekolah">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="school_address_conselor">Alamat Sekolah</label>
                                <input type="text" class="form-control" id="school_address_conselor" placeholder="Alamat Sekolah">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="employee_status_conselor">Status Kepegawaian</label>
                                <input type="text" class="form-control" id="employee_status_conselor" placeholder="Status Kepegawaian">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="nik_conselor">NIK</label>
                                <input type="number" class="form-control" id="nik_conselor" placeholder="NIK">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="year_of_service_conselor">Masa Kerja</label>
                                <input type="text" class="form-control" id="year_of_service_conselor" placeholder="Masa Kerja">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="class_name_conselor">Kelas Bimbingan atau <a href="javascript:void(0)"
                                data-toggle="modal" data-target="#modal-create-class">Buat Baru</a></label>
                                <select id="class_name_conselor" class="form-control"
                                onchange="add_class_conselor(this.value)">
                                </select>
                                <div id="class_name_conselor_list"></div>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="additional_task_conselor">Tugas Tambahan</label>
                                <input type="text" class="form-control" id="additional_task_conselor" placeholder="Tugas Tambahan">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                <li>
                                    <a href="javascript:void(0)" class="btn btn-lg btn-danger"
                                    id="btn-save">Simpan</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        '
    ));
?>