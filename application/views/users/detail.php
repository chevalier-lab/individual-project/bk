<?php
    $jsDir = base_url() . "assets/js/apps/";

    $this->load->view("templates/base", array(
        "title" => "Detail Pengguna - BK SMK Telkom",
        "additional_foot" => '
            <script src="'.$jsDir.'users/detail.js"></script>
        ',
        "components" => array(
            "users/components/modal-change-password-user"
        ),
        "content" => '

        <div class="nk-block-head nk-block-head-lg">
            <div class="nk-block-head-sub"><span>Kelolah Pengguna</span></div>
            <div class="nk-block-between-md g-4">
                <div class="nk-block-head-content">
                    <h2 class="nk-block-title fw-normal">Detail Pengguna</h2>
                    <div class="nk-block-des">
                        <p>Detail pengguna. <span class="text-danger"><em class="icon ni ni-info"></em></span></p>
                    </div>
                </div>
                <div class="nk-block-head-content">
                    <ul class="nk-block-tools gx-3">
                        <li class="order-md-last"><a href="javascript:void(0);" class="btn btn-white btn-dim btn-outline-danger"
                        data-toggle="modal" data-target="#modal-change-password-user"><span>
                        Ganti Password
                        </span></a></li>
                    </ul>
                </div>
            </div>
        </div><!-- .nk-block-head -->

        <input type="hidden" id="id_m_users" value="'.$id.'">

        <div class="card card-bordered">
            <div class="card-inner">
                <form class="gy-4" id="form-save-user">
                    <div class="row">
                        <div class="col-sm-12">
                            <strong>Biodata Pengguna</strong>
                        </div>
                    </div>

                    <div class="row" id="container-biodata">
                        
                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="full_name">Nama Lengkap</label>
                                <input type="text" class="form-control" id="full_name" placeholder="Masukkan Nama Lengkap">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="call_name">Nama Panggilan</label>
                                <input type="text" class="form-control" id="call_name" placeholder="Masukkan Nama Panggilan">
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="gender">Jenis Kelamin</label>
                                <select id="gender" class="form-control">
                                    <option value="" selected>Pilih Jenis Kelamin</option>
                                    <option value="Pria">Pria</option>
                                    <option value="Wanita">Wanita</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="religion">Agama</label>
                                <select id="religion" class="form-control">
                                    <option value="Islam">Islam</option>
                                    <option value="Kristen Protestan">Kristen Protestan</option>
                                    <option value="Katolik">Katolik</option>
                                    <option value="Hindu">Hindu</option>
                                    <option value="Buddha">Buddha</option>
                                    <option value="Kong Hu Cu">Kong Hu Cu</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-6 col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="phone_number">Nomor Telepon</label>
                                <input type="number" class="form-control" id="phone_number" placeholder="Masukkan Nomor Telepon">
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-label" for="address">Alamat</label>
                                <input type="text" class="form-control" id="address" placeholder="Masukkan Alamat">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                <li>
                                    <a href="javascript:void(0)" class="btn btn-lg btn-danger"
                                    id="btn-save">Simpan</a>
                                </li>
                            </ul>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        '
    ));
?>