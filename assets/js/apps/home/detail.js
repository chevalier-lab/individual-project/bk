"use strict";

var post_id;

!function (NioApp, $) {
    "use strict";

    post_id = $("#id_post").val();

    loadDetailArticle();

}(NioApp, jQuery);

function loadDetailArticle() {
    // Do Load Room
  global.getRAW(
    global.base_url + "/services/post/detail/" + post_id + "?tokenize=" + global.csrf_token, function(res) {

      if (res.code == 200) {

        var post = res.data;
          
        $("#post-title").html(post.title);
        $("#post-meta").html(`
            <p>
                Artikel dibuat oleh <strong>${post.full_name}</strong> pada tanggal <strong>${global.date(post.updated_at)}</strong>. <span class="text-primary"><em class="icon ni ni-info"></em></span>
                <br> Kategori : <span class="badge badge-primary">${post.category}</span>
            </p>
        `);
        $("#detail-post").html(`
            <div>
                <div style="max-width: 100%; height: 200px;
                background-size: cover;
                background-position: center;
                background-image: url('${post.uri}')"></div>                
                <div class="my-2">
                ${post.content}
                </div>
            </div>
        `);

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}