"use strict";

var page = 0;
var posts = [];

!function (NioApp, $) {
  "use strict";

    loadArticle();

}(NioApp, jQuery);

function setupArticle()
{
    $("#list-post").html(`
    <div class="row">
    ${posts.map(function(item) {
        var target = global.base_url + "/views/homeDetail/" + item.id;
        return `
        <div class="col-md-6">
            <div class="card card-bordered"
            style="cursor: pointer"
            onclick="location.assign('${target}')">
                <div class="card-inner">
                    <div class="align-center flex-wrap flex-md-nowrap g-4">
                        <div class="nk-block-image w-120px flex-shrink-0" style="height: 120px;
                        background-size: cover;
                        background-position: center;
                        background-image: url('${item.uri}')">
                        </div>
                        <div class="nk-block-content">
                            <div class="nk-block-content-head">
                                <div class="h6">${item.title}</div>
                                <p class="text-soft">${item.full_name} | Kategori: ${item.category}</p>
                            </div>
                        </div>
                    </div>
                </div><!-- .card-inner -->
            </div><!-- .card -->
        </div>
        `;
    }).join('')}
    </div>
    `);
}

function loadArticle() 
{
    var formData = new FormData();
    formData.append("page", page);
    formData.append("search", "");

    // Do Create Class
    global.postRAW(formData, 
        global.base_url + "/services/post/all?tokenize=" + global.csrf_token, function(res) {

          if (res.code == 200) {
              
            posts = res.data;
            setupArticle()
  
            return
          }
  
          NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
        });
}