"use strict";

var room = [];
var chats = [];
var selectedRoom = null;
var page = 0;
var state = 0;

!function (NioApp, $) {
  "use strict";

  loadRoom();
  
  setupState();

  $("#btn-send-chat").on("click", function() {
    $("#btn-send-chat").attr("disabled", true);
    $("#btn-send-chat").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    sendChatContent();
  });

  $("#btn-save-room-group").on("click", function() {
    if (state == 0) {
      state = 1;
      setupState();
    } else {
      state = 0;
      $("#btn-save-room-group").attr("disabled", true);
      $("#btn-save-room-group").html(`
          <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
          <span> Loading... </span>
      `);

      doCreateChatGroup();
    }
  });

}(NioApp, jQuery);

function setupState() {
  if (state == 0) {
    $("#container-create-room-group").show();
    $("#form-create-room-group").hide();
    $("#btn-save-room-group").text("Selanjutnya");
  } else {
    $("#container-create-room-group").hide();
    $("#form-create-room-group").show();
    $("#btn-save-room-group").text("Simpan");
  }
}

function doCreateChatGroup() {
  var raw = global.raw({
    id_master: selectedRoom.id_konselor,
    id_request: selectedRoom.id_konseli,
    title: $("#room-group-title").val(),
    description: $("#room-group-description").val()
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
    global.base_url + "/services/groupChat/create?tokenize=" + global.csrf_token, function(res) {

      if (res.code == 200) {

        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

        setupState();

        setTimeout(() => {
          
        location.assign(global.base_url + "/views/roomChatConselor")
        }, 1000);

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function setupChatDetail() {
  $("#list-chat").html(`
    ${chats.map(function(item) {
      if (item.content != null)
      return `
      <div class="nk-reply-item">
        <div class="nk-reply-header">
            <div class="user-card">
                <div class="user-name">${item.full_name}</div>
            </div>
            <div class="date-time">${global.date(item.created_at)}</div>
        </div>
        <div class="nk-reply-body ml-0"
        style="background-color: #f5f6fa;">
          <div class="p-2">${item.content}</div>
        </div>
    </div><!-- .nk-reply-item -->
      `;

      if (item.id_photo != null)
      return `
      <div class="nk-reply-item">
        <div class="nk-reply-header">
            <div class="user-card">
                <div class="user-name">${item.full_name}</div>
            </div>
            <div class="date-time">${global.date(item.created_at)}</div>
        </div>
        <div class="nk-reply-body ml-0"
        style="background-color: #f5f6fa;">
          <div class="row p-2">
            <div class="col-sm-5">
              <img src="${item.photo_uri}" style="max-width: 100%" /><br>
            </div>
            <div class="col-sm-7">
            ${item.photo_file_name}
            </div>
          </div>
        </div>
    </div><!-- .nk-reply-item -->
      `;

      if (item.id_document != null)
      return `
      <div class="nk-reply-item">
        <div class="nk-reply-header">
            <div class="user-card">
                <div class="user-name">${item.full_name}</div>
            </div>
            <div class="date-time">${global.date(item.created_at)}</div>
        </div>
        <div class="nk-reply-body ml-0"
        style="background-color: #f5f6fa;">
          <div class="p-2">
          <ul class="attach-list" style="m-0">
              <li class="attach-item">
                  <a class="download" href="${item.document_uri}"
                  target="_blank"><em class="icon ni ni-file"></em><span>${item.document_file_name}</span></a>
              </li>
          </ul>
          </div>
        </div>
    </div><!-- .nk-reply-item -->
      `;
    }).join('')}
  `);
}

function loadChatDetail() {
  var formData = new FormData()
  formData.append("page", page);

  // Do Create Class
  global.postRAW(formData, 
    global.base_url + "/services/personalChat/loadAllChatContent/" + selectedRoom.id + "?tokenize=" + global.csrf_token, function(res) {

      if (res.code == 200) {

        chats = res.data;
        setupChatDetail();

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function uploadImage(image, callback) {
  var formData = new FormData();
  formData.append("photo", image);

  global.postRAW(formData, 
      global.base_url + "/services/medias/create?tokenize=" + global.csrf_token, function(res) {

        if (res.code == 200) {

          callback(res.data);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function sendChatPhoto(input) {
  if (input.files && input.files[0]) {
    uploadImage(input.files[0], function(res) {

      var raw = global.raw({
        id_photo: res.id,
        id_room: selectedRoom.id
      });
    
      var formData = new FormData()
      formData.append("raw", raw);
    
      // Do Create Class
      global.postRAW(formData, 
          global.base_url + "/services/personalChat/send?tokenize=" + global.csrf_token, function(res) {
    
            if (res.code == 200) {
    
              NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

              loadChatDetail();
    
              return
            }
    
            NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
          });

    });
  }
}

function sendChatDocument(input) {
  if (input.files && input.files[0]) {
    uploadImage(input.files[0], function(res) {

      var raw = global.raw({
        id_document: res.id,
        id_room: selectedRoom.id
      });
    
      var formData = new FormData()
      formData.append("raw", raw);
    
      // Do Create Class
      global.postRAW(formData, 
          global.base_url + "/services/personalChat/send?tokenize=" + global.csrf_token, function(res) {
    
            if (res.code == 200) {
    
              NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

              loadChatDetail();
    
              return
            }
    
            NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
          });

    });
  }
}

function sendChatContent() {
  var raw = global.raw({
    content: $("#chat-content").val(),
    id_room: selectedRoom.id
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/personalChat/send?tokenize=" + global.csrf_token, function(res) {

        $("#btn-send-chat").removeAttr("disabled");
        $("#btn-send-chat").html(`Simpan`);

        if (res.code == 200) {

          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
          $("#chat-content").val("")
          loadChatDetail();

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function setSelectedRoom(room_id) {
  var position = -1;
  room.forEach(function(item, index) {
    if (item.id == room_id) position = index;
  });

  if (position != -1) {
    selectedRoom = room[position];
    setupRoom();
    loadChatDetail();
  }
}

function setupRoom() {
  $("#chat-partner-name").html(`${selectedRoom.konseli_name}`);
  
  $("#list-room").html(`${room.map(function(item, position) {
    var avatar = ((position + 1) >= 10) ? (position + 1).toString() : "0" + (position + 1);
    var selected = "is-unread";

    if (selectedRoom != null) {
      if (selectedRoom.id == item.id) {
        selected = "current";
      }
    }

    return `
    <div class="nk-msg-item ${selected}" data-msg-id="3"
    onclick="setSelectedRoom(${item.id})">
      <div class="nk-msg-media user-avatar bg-purple">
          <span>${avatar}</span>
      </div>
      <div class="nk-msg-info">
          <div class="nk-msg-from">
              <div class="nk-msg-sender">
                  <div class="name">${item.konseli_name}</div>
              </div>
              <div class="nk-msg-meta">
                <div class="unread"><span class="badge badge-primary">2</span></div>
              </div>
          </div>
          <div class="nk-msg-context">
              <div class="nk-msg-text">
                <div class="date">${global.date(item.updated_at)}</div>
              </div>
          </div>
      </div>
  </div><!-- .nk-msg-item -->
    `;
  }).join('')}`);
}

function loadRoom() {
  // Do Load Room
  global.getRAW(
    global.base_url + "/services/personalChat/allKonseli?tokenize=" + global.csrf_token, function(res) {

      if (res.code == 200) {
          
        room = res.data;
        if (room.length > 0) {
          selectedRoom = room[0];
          setSelectedRoom(selectedRoom.id)
        }

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}