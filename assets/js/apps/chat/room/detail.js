"use strict";

var participants = [];
var chats = [];
var selectedRoom = null;
var page = 0;
var room_id;
var classNames = [];
var students = [];

!function (NioApp, $) {
  "use strict";

  room_id = $("#room_id").val();

  var available = $("#available").val();

  if (available == 0) {
    $(".nk-block-tools").hide();
  }

  loadParticipant();

  loadDetail();

  loadClassName();

  $("#btn-send-chat").on("click", function() {
    $("#btn-send-chat").attr("disabled", true);
    $("#btn-send-chat").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    sendChatContent();
  });

  $("#btn-save-invite-member").on("click", function() {
    $("#btn-save-invite-member").attr("disabled", true);
    $("#btn-save-invite-member").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doInvitation();
  });

}(NioApp, jQuery);

function doInvitation() {
  if ($("#invite-member-student").val() == "") {
    NioApp.Toast("Harap memilih siswa terlebih dahulu", 'error', {position: 'bottom-right'});
    return 
  }

  var raw = global.raw({
    id_room: room_id,
    id_m_users: $("#invite-member-student").val()
  });

  var formData = new FormData();
  formData.append("raw", raw);

  global.postRAW(formData, 
    global.base_url + "/services/groupChat/invite?tokenize=" + global.csrf_token, function(res) {

      $("#btn-save-invite-member").removeAttr("disabled");
      $("#btn-save-invite-member").html(`Simpan`);

      if (res.code == 200) {

        $("#invite-member")[0].reset();
        $("#modal-invite-member").modal('toggle');
        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

        setTimeout(() => {
          location.reload();
        }, 1000);

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function loadSiswa(id) {
  global.getRAW(global.base_url + "/services/users/konseli_by_class/" + id + "?tokenize=" + global.csrf_token, function(res) {
    if (res.code == 200) {
      students = res.data;
      setupStudent();
      return
    }

    NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function setupStudent() {
  $("#invite-member-student").html(`
  <option value="" disabled selected>Pilih Siswa</option>
  ${students.map(function(item) {
    return `<option value="${item.id}">${item.full_name} (${item.nis})</option>`;
  }).join('')}`);
}

function setupClassName() {
  $("#invite-member-class").html(`
  <option value="" disabled selected>Pilih Kelas</option>
  ${classNames.map(function(item) {
    return `<option value="${item.id}">${item.class_name} (${item.class_code})</option>`;
  }).join('')}`);
}

function loadClassName() {
  global.getRAW(global.base_url + "/services/className/all?tokenize=" + global.csrf_token, function(res) {
    if (res.code == 200) {
      classNames = res.data;
      setupClassName();
      return
    }

    NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function loadDetail() {
  // Do Load Room
  global.getRAW(
    global.base_url + "/services/groupChat/detail/" + room_id + "?tokenize=" + global.csrf_token, function(res) {

      if (res.code == 200) {
          
        var data = res.data;
        $("#chat-title").html(data.title);
        $("#chat-description").html(global.date(data.updated_at));

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function loadParticipant() {
  // Do Load Room
  global.getRAW(
    global.base_url + "/services/groupChat/participant/" + room_id + "?tokenize=" + global.csrf_token, function(res) {

      if (res.code == 200) {
          
        participants = res.data;
        setupParticipant();

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function setupParticipant() {
  $("#list-room").html(`
  <div class="p-2">
  <h5>Peserta Group</h5>
  </div>
  ${participants.map(function(item, position) {
    var avatar = ((position + 1) >= 10) ? (position + 1).toString() : "0" + (position + 1);
    var selected = "is-unread";

    return `
    <div class="nk-msg-item ${selected}" data-msg-id="3">
      <div class="nk-msg-media user-avatar bg-purple">
          <span>${avatar}</span>
      </div>
      <div class="nk-msg-info">
          <div class="nk-msg-from">
              <div class="nk-msg-sender">
                  <div class="name">${item.full_name}</div>
              </div>
          </div>
          <div class="nk-msg-context">
              <div class="nk-msg-text">
                <div class="date">${global.date(item.updated_at)}</div>
              </div>
          </div>
      </div>
  </div><!-- .nk-msg-item -->
    `;
  }).join('')}`);

  loadChatDetail();
}

function setupChatDetail() {
  $("#list-chat").html(`
    ${chats.map(function(item) {
      if (item.content != null)
      return `
      <div class="nk-reply-item">
        <div class="nk-reply-header">
            <div class="user-card">
                <div class="user-name">${item.full_name}</div>
            </div>
            <div class="date-time">${global.date(item.created_at)}</div>
        </div>
        <div class="nk-reply-body ml-0"
        style="background-color: #f5f6fa;">
          <div class="p-2">${item.content}</div>
        </div>
    </div><!-- .nk-reply-item -->
      `;

      if (item.id_photo != null)
      return `
      <div class="nk-reply-item">
        <div class="nk-reply-header">
            <div class="user-card">
                <div class="user-name">${item.full_name}</div>
            </div>
            <div class="date-time">${global.date(item.created_at)}</div>
        </div>
        <div class="nk-reply-body ml-0"
        style="background-color: #f5f6fa;">
          <div class="row p-2">
            <div class="col-sm-5">
              <img src="${item.photo_uri}" style="max-width: 100%" /><br>
            </div>
            <div class="col-sm-7">
            ${item.photo_file_name}
            </div>
          </div>
        </div>
    </div><!-- .nk-reply-item -->
      `;

      if (item.id_document != null)
      return `
      <div class="nk-reply-item">
        <div class="nk-reply-header">
            <div class="user-card">
                <div class="user-name">${item.full_name}</div>
            </div>
            <div class="date-time">${global.date(item.created_at)}</div>
        </div>
        <div class="nk-reply-body ml-0"
        style="background-color: #f5f6fa;">
          <div class="p-2">
          <ul class="attach-list" style="m-0">
              <li class="attach-item">
                  <a class="download" href="${item.document_uri}"
                  target="_blank"><em class="icon ni ni-file"></em><span>${item.document_file_name}</span></a>
              </li>
          </ul>
          </div>
        </div>
    </div><!-- .nk-reply-item -->
      `;
    }).join('')}
  `);
}

function loadChatDetail() {
  var formData = new FormData()
  formData.append("page", page);

  // Do Create Class
  global.postRAW(formData, 
    global.base_url + "/services/groupChat/loadAllChatContent/" + room_id + "?tokenize=" + global.csrf_token, function(res) {

      if (res.code == 200) {

        chats = res.data;
        setupChatDetail();

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function uploadImage(image, callback) {
  var formData = new FormData();
  formData.append("photo", image);

  global.postRAW(formData, 
      global.base_url + "/services/medias/create?tokenize=" + global.csrf_token, function(res) {

        if (res.code == 200) {

          callback(res.data);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function sendChatPhoto(input) {
  if (input.files && input.files[0]) {
    uploadImage(input.files[0], function(res) {

      var raw = global.raw({
        id_photo: res.id,
        id_room: room_id
      });
    
      var formData = new FormData()
      formData.append("raw", raw);
    
      // Do Create Class
      global.postRAW(formData, 
          global.base_url + "/services/groupChat/send?tokenize=" + global.csrf_token, function(res) {
    
            if (res.code == 200) {
    
              NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

              loadChatDetail();
    
              return
            }
    
            NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
          });

    });
  }
}

function sendChatDocument(input) {
  if (input.files && input.files[0]) {
    uploadImage(input.files[0], function(res) {

      var raw = global.raw({
        id_document: res.id,
        id_room: room_id
      });
    
      var formData = new FormData()
      formData.append("raw", raw);
    
      // Do Create Class
      global.postRAW(formData, 
          global.base_url + "/services/groupChat/send?tokenize=" + global.csrf_token, function(res) {
    
            if (res.code == 200) {
    
              NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

              loadChatDetail();
    
              return
            }
    
            NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
          });

    });
  }
}

function sendChatContent() {
  var raw = global.raw({
    content: $("#chat-content").val(),
    id_room: room_id
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/groupChat/send?tokenize=" + global.csrf_token, function(res) {

        $("#btn-send-chat").removeAttr("disabled");
        $("#btn-send-chat").html(`Simpan`);

        if (res.code == 200) {

          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
          $("#chat-content").val("")
          loadChatDetail();

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

