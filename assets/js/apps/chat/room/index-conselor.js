"use strict";

var room = [];
var chats = [];
var selectedRoom = null;
var page = 0;

!function (NioApp, $) {
  "use strict";

  loadRoom();

}(NioApp, jQuery);

function setSelectedRoom(room_id) {
  var position = -1;
  room.forEach(function(item, index) {
    if (item.id == room_id) position = index;
  });

  if (position != -1) {
    selectedRoom = room[position];
    setupDetailRoom()
  }
}

function setupRoom() {
  $("#list-room").html(`
  <div class="card card-bordered card-full col-sm-12">
    <div class="card-inner-group">
  ${room.map(function(item, position) {
    var avatar = ((position + 1) >= 10) ? (position + 1).toString() : "0" + (position + 1);
    var selected = "is-unread";
    var status = (item.is_accept == 1 || item.is_accept == "1") ? "success" : "danger";
    var statusText = (item.is_accept == 1 || item.is_accept == "1") ? "Bergabung" : "Belum Bergabung";

    return `
    
    <div class="card-inner card-inner-md">
        <div class="user-card">
            <div class="user-avatar bg-primary-dim">
                <span>${avatar}</span>
            </div>
            <div class="user-info">
                <span class="lead-text">${item.title}</span>
                <span class="sub-text">
                    <div>Bersama: ${item.konselor_name}</div>
                    <div>
                        <span class="badge badge-${status}">${statusText}</span>
                        ${global.date(item.updated_at)}
                    </div>
                </span>
            </div>
            <div class="user-action">
                <div class="drodown">
                    <a href="#" class="dropdown-toggle btn btn-icon btn-trigger mr-n1" data-toggle="dropdown" aria-expanded="false"><em class="icon ni ni-more-h"></em></a>
                    <div class="dropdown-menu dropdown-menu-right" style="">
                        <ul class="link-list-opt no-bdr">
                            <li><a href="javascript:void(0);"
                            data-toggle="modal" data-target="#modal-preview-room"
                            onclick="setSelectedRoom(${item.id})"><em class="icon ni ni-eye"></em><span>Detail Konseling</span></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    `;
  }).join('')}
  </div>
    </div>`);
}

function setupDetailRoom() {
    var item = selectedRoom;
    var status = (item.is_accept == 1 || item.is_accept == "1") ? "success" : "danger";
    var statusText = (item.is_accept == 1 || item.is_accept == "1") ? "Bergabung" : "Belum Bergabung";

    var button = `
        <a href="${global.base_url + "/views/roomChatDetail/" + item.id}" class="btn btn-primary">Mulai konseling</a>
    `;
    if (item.is_accept == 0 || item.is_accept == "0") {
        
        var button = `
            <button type="button" class="btn btn-primary" 
            onclick="doAcceptInvitation(${item.id})">Ya, saya setuju</button>
        `;
    }

    $("#preview-room").html(`
        <div class="col-sm-12">
          <div>
              <div class="user-info">
                  <span class="lead-text">${item.title}</span>
                  <span class="sub-text">
                      <div>Bersama: ${item.konselor_name}</div>
                      <div>
                          <span class="badge badge-${status}">${statusText}</span>
                          ${global.date(item.updated_at)}
                      </div>
                  </span>
              </div>
          </div>
          <p class="mt-2">${item.description}</p>
          <h4><center>Persyaratan Konseli untuk Bergabung di Konseling Kelompok</center></h4>
          <div class="py-1">
            <strong>Peran Anggota Kelompok</strong>
          </div>
          <p>Masing-masing anggota kelompok beraktifitas langsung dan mandiri dalam bentuk:</p>
          <ol>
              <li>1. Aktif, mandiri melaui aktivitas langsung melalui sikap 3M (mendengar dengan aktif, memahami dengan positif dan merespon dengan tepat dan positif), sikap seperti seorang konselor.</li>
              <li>2. Berpikir dan berbagi pendapat, ide dan pengalaman</li>
              <li>3. Merasa, berempati dan bersikap</li>
              <li>4. Menganalisa, mengkritisi dan berargumentasi</li>
              <li>5. Berpartisipasi dalam kegiatan bersama</li>
          </ol>
          <div class="py-1">
            <strong></strong>
          </div>
          <p>Aktifitas mandiri masing-masing anggota kelompok diorientasikan pada kehidupan bersama dalam kelompok. Kebersamaan ini diwujudkan dalam:</p>
          <ol>
              <li>1. Aktif membina keakraban, membina keikatan emosional</li>
              <li>2. Mematuhi etika kelompok</li>
              <li>3. Menjaga kerahasiaan, perasaan dan membantu serta</li>
              <li>4. Membina kelompok untuk menyukseskan kegiatan kelompok.</li>
          </ol>
        </div>

        <div class="col-sm-12 mt-2">
            ${button}
        </div>
    `);
}

function doAcceptInvitation(id) {
    var target = global.base_url + "/services/groupChat/accept/" + id + "?tokenize=" + global.csrf_token;
    // Do Load Room
  global.getRAW(
    target, function(res) {

      if (res.code == 200) {
          
        NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
        location.assign(global.base_url + "/views/roomChatDetail/" + id)

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function loadRoom() {
  // Do Load Room
  global.getRAW(
    global.base_url + "/services/groupChat/allKonseli?tokenize=" + global.csrf_token, function(res) {

      if (res.code == 200) {
          
        room = res.data;
        setupRoom();

        return
      }

      NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}