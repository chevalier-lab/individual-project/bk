"use strict";

var table = null;

!function (NioApp, $) {
  "use strict";

    loadChatList();

    loadChatGroupList();

}(NioApp, jQuery);

function loadChatList() 
{

    if (table != null) {
        table.destroy();
    }

    table = NioApp.DataTable('.chat-list', {
        responsive: true,            
        processing: true,            
        serverSide: true,            
        ordering: true, 
        paging: true,
        pageLength: 5,
        bDestroy: true,
        order: [[ 0, 'desc' ]], 
        ajax: {                
            url: global.base_url + "/services/personalChat/index?tokenize=" + global.csrf_token, 
            type: "POST"
        },
        deferRender: true,            
        pagingType: "full_numbers",
        lengthMenu: [5, 10, 25, 50, 75, 100],
        columns: [
            {
                render: function(data, type, row) {                    
                    return `
                        <button class="btn btn-primary btn-sm" type="button"
                        onclick="doDeleteUser(${row.id}, '${row.token}')">
                            <em class="icon ni ni-trash"></em> 
                            <span>Hapus</span>
                        </button>
                    `;
                }
            },
            {data: "token"},
            {data: "konseli_name"},
            {data: "konselor_name"}
        ]
    });
}

function doDeleteUser(id, name) {
    Swal.fire({
        title: 'Apa anda yakin?',
        text: "Anda akan menghapus " + name,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus!'
      }).then((result) => {
        if (result.isConfirmed) {
            var target = global.base_url + "/services/personalChat/remove/" + id + "?tokenize=" + global.csrf_token;
            global.getRAW(target, function(res) {
                if (res.code == 200) {
                    NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
                    loadChatList() 
                  return
                }
            
                NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
              });
        }
      })

    
}

function loadChatGroupList() 
{

    if (table != null) {
        table.destroy();
    }

    table = NioApp.DataTable('.chat-group-list', {
        responsive: true,            
        processing: true,            
        serverSide: true,            
        ordering: true, 
        paging: true,
        pageLength: 5,
        bDestroy: true,
        order: [[ 0, 'desc' ]], 
        ajax: {                
            url: global.base_url + "/services/groupChat/index?tokenize=" + global.csrf_token, 
            type: "POST"
        },
        deferRender: true,            
        pagingType: "full_numbers",
        lengthMenu: [5, 10, 25, 50, 75, 100],
        columns: [
            {
                render: function(data, type, row) {                    
                    return `
                        <button class="btn btn-primary btn-sm" type="button"
                        onclick="doDeleteUserGroup(${row.id}, '${row.token}')">
                            <em class="icon ni ni-trash"></em> 
                            <span>Hapus</span>
                        </button>
                    `;
                }
            },
            {data: "token"},
            {data: "konselor_name"},
            {data: "title"},
            {data: "created_at"}
        ]
    });
}

function doDeleteUserGroup(id, name) {
    Swal.fire({
        title: 'Apa anda yakin?',
        text: "Anda akan menghapus " + name,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus!'
      }).then((result) => {
        if (result.isConfirmed) {
            var target = global.base_url + "/services/groupChat/remove/" + id + "?tokenize=" + global.csrf_token;
            global.getRAW(target, function(res) {
                if (res.code == 200) {
                    NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
                    loadChatGroupList() 
                  return
                }
            
                NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
              });
        }
      })
}