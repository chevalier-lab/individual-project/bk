"use strict";

console.log("General JS");

function showTogglePassword() {
    var x = document.getElementById("password");
    if (x.type === "password") x.type = "text";
    else x.type = "password";
}

function showToggleDisabled(id) {
    var x = document.getElementById(id);
    if (x.disabled) x.disabled = false;
    else x.disabled = true;
}

var global = {
    base_url: $("#base_url").val(),
    csrf_token: $("#csrf_token").val(),
    data: function(response) {
        return JSON.parse(response)
    },
    raw: function(obj) {
        return JSON.stringify(obj)
    },
    money: function(angka, prefix){
        var number_string = angka.replace(/[^,\d]/g, '').toString(),
        split   		= number_string.split(','),
        sisa     		= split[0].length % 3,
        rupiah     		= split[0].substr(0, sisa),
        ribuan     		= split[0].substr(sisa).match(/\d{3}/gi);
    
        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }
    
        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp ' + rupiah : '');
    },
    date: function(date="") {
        var dateSplit = date.split("-");
        var month = dateSplit[1];
        var bulan = ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", 
        "Agustus", "September", "Oktober", "November", "Desember"];
        month = bulan[Number(month) - 1];
        dateSplit[1] = month;
        dateSplit = dateSplit.join(" ");
        dateSplit = dateSplit.split(" ");
        return dateSplit[2] + " " + dateSplit[1] + " " + dateSplit[0] + " " + dateSplit[3];
    },
    capitalize: function(string) {
        var splitStr = string.split(' ')
        var fullStr = '';
    
        $.each(splitStr,function(index){
            var currentSplit = splitStr[index].charAt(0).toUpperCase() + splitStr[index].slice(1);
            fullStr += currentSplit + " "
        });
    
        return fullStr;
    },
    random: function (min, max, step) {
        const randomNum = min + Math.random() * (max - min);
        return Math.round(randomNum / step) * step;
    }
}

// ============ GENERAL

// POST
global.postRAW = function(req, url, callback) {
    $.ajax({
        url: url,
        data: req,
        type: "POST",
        contentType: false,
        processData: false,
        success: function(response) {
            response = global.data(response)
            callback(response);
        }
    });
}

// GET
global.getRAW = function(url, callback) {
    $.ajax({
        url: url,
        data: null,
        type: "GET",
        contentType: false,
        processData: false,
        success: function(response) {
            response = global.data(response)
            callback(response);
        }
    });
}