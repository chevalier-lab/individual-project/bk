"use strict";

!function (NioApp, $) {
  "use strict";

  // Load Profile
  loadProfile()

  // Load Notification
  loadNotification();
}(NioApp, jQuery);

function loadProfile() {
    // Setup Before
    $("#sidebar-profile").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);
    $("#topbar-profile").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);
    $("#welcome-profile").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    // Process
    global.getRAW(global.base_url + "/services/general/profile?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

            var face = `<em class="icon ni ni-user-alt"></em>`;
            if (res.data.id_m_medias != null) face = `<img src="${res.data.uri}" style="width: 24px; height: 24px" />`;

            // Setup Topbar Profile
            $("#topbar-profile").html(`
            <div class="user-avatar sm">
                ${face}
            </div>
            <div class="user-info d-none d-md-block">
                <div class="user-status">${res.data.type}</div>
                <div class="user-name dropdown-indicator">${res.data.full_name}</div>
            </div>
            `);

            // Setup Topbar Profile
            $("#topbar-card-profile").html(`
            <div class="user-avatar">
                ${face}
            </div>
            <div class="user-info">
                <span class="lead-text">${res.data.full_name}</span>
                <span class="sub-text">${res.data.phone_number}</span>
            </div>
            `);
            
            return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}

function loadNotification() {
    // Process
    global.getRAW(global.base_url + "/services/notification/all?tokenize=" + global.csrf_token, 
    function(res) {
        if (res.code == 200) {

            if (res.data.length == 0) {
                $("#notification-list").html(`
                    <div class="p-2">
                        <center>Belum ada notifikasi</center>
                    </div>
                `);
            } else {
                $("#notification-list").html(`${res.data.map(function(item) {
                    return `
                    <div class="nk-notification-item dropdown-inner"
                    onclick="location.assign('${item.params}')">
                        <div class="nk-notification-icon">
                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                        </div>
                        <div class="nk-notification-content">
                            <div class="nk-notification-text">${item.content}</div>
                            <div class="nk-notification-time">${global.date(item.updated_at)}</div>
                        </div>
                    </div>
                    `;
                }).join('')}`);
            }
            
            return
        }
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
    });
}