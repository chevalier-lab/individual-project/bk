"use strict";

var table = null;

!function (NioApp, $) {
  "use strict";

    loadUsers();

}(NioApp, jQuery);

function loadUsers() 
{

    if (table != null) {
        table.destroy();
    }

    table = NioApp.DataTable('.user-list', {
        responsive: true,            
        processing: true,            
        serverSide: true,            
        ordering: true, 
        paging: true,
        pageLength: 5,
        bDestroy: true,
        order: [[ 0, 'desc' ]], 
        ajax: {                
            url: global.base_url + "/services/users/index?tokenize=" + global.csrf_token, 
            type: "POST"
        },
        deferRender: true,            
        pagingType: "full_numbers",
        lengthMenu: [5, 10, 25, 50, 75, 100],
        columns: [
            {
                render: function(data, type, row) {                    
                    return `
                        <button class="btn btn-primary btn-sm" type="button"
                        onclick="doDeleteUser(${row.id}, '${row.full_name}')">
                            <em class="icon ni ni-trash"></em> 
                            <span>Hapus</span>
                        </button>
                    `;
                }
            },
            {data: "full_name"},
            {
                render: function(data, type, row) {
                    if (row.phone_number == null) return "-";
                    else return row.phone_number;
                }
            },
            {data: "gender"},
            {data: "religion"},
            {
                render: function(data, type, row) {
                    var status = ``;
                    if (row.type == "administrator") status = `<span class="badge badge-success">Administrator</span>`;
                    else if (row.type == "konseli") status = `<span class="badge badge-info">Konseli</span>`;
                    else if (row.type == "konselor") status = `<span class="badge badge-primary">Konselor</span>`;
                    return status;
                }
            },
            {data: "created_at"},
            {data: "updated_at"},
            {
                render: function(data, type, row) {
                    var target = "";
                    if (row.type == "administrator")
                    target = global.base_url + "/views/usersDetail/" + row.id + "?tokenize=" + global.csrf_token
                    else if (row.type == "konseli")
                    target = global.base_url + "/views/usersDetailConseli/" + row.id + "?tokenize=" + global.csrf_token
                    else if (row.type == "konselor")
                    target = global.base_url + "/views/usersDetailConselor/" + row.id + "?tokenize=" + global.csrf_token
                    
                    return `
                        <button class="btn btn-primary btn-sm" type="button"
                        onclick="location.assign('${target}')">
                            <em class="icon ni ni-eye-fill"></em> 
                            <span>Lihat</span>
                        </button>
                    `;
                }
            }
        ]
    });
}

function doDeleteUser(id, name) {
    Swal.fire({
        title: 'Apa anda yakin?',
        text: "Anda akan menghapus " + name,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Ya, hapus!'
      }).then((result) => {
        if (result.isConfirmed) {
            var target = global.base_url + "/services/users/bannedUser/" + id + "?tokenize=" + global.csrf_token;
            global.getRAW(target, function(res) {
                if (res.code == 200) {
                    NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
                    loadUsers() 
                  return
                }
            
                NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
              });
        }
      })

    
}