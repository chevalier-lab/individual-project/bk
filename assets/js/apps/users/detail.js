"use strict";

var classNames = [];

!function (NioApp, $) {
  "use strict";

  $("#btn-change-password-user").on("click", function() {
    $("#btn-change-password-user").attr("disabled", true);
    $("#btn-change-password-user").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doChangePassword();
  });

  $("#btn-save").on("click", function() {
    $("#btn-save").attr("disabled", true);
    $("#btn-save").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doSaveUser();
  });

  $("#btn-modal-change-photo").on("click", function() {
    $("#btn-modal-change-photo").attr("disabled", true);
    $("#btn-modal-change-photo").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doChangePhoto();
  });

  $("#modal-change-photo-input").change(function() {
    readURL(this);
  });

  loadUser();

}(NioApp, jQuery);

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#modal-change-photo-preview').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function doChangePhoto() {
  var file = $("#modal-change-photo-input").prop('files')[0];
  if (file == null) {
    $("#btn-modal-change-photo").removeAttr("disabled");
    $("#btn-modal-change-photo").html(`Simpan`);

    NioApp.Toast("Harap memilih foto terlebih dahulu", 'error', {position: 'bottom-right'});
    return
  }

  uploadImage(file, function(media) {
    var id_m_medias = media.id;
    var id = $("#id_m_users").val();
    var target = global.base_url + "/services/users/setUserFace/"+id+"?id_m_medias="+id_m_medias+"&tokenize=" + global.csrf_token
    // Do Load Konselor
    global.getRAW(
      target, function(res) {
        $("#btn-modal-change-photo").removeAttr("disabled");
        $("#btn-modal-change-photo").html(`Simpan`);

        if (res.code == 200) {

          $("#form-change-photo")[0].reset();
          $("#modal-change-photo").modal('toggle');
          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

          setTimeout(() => {
            location.reload();
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
  })
}

function doChangePassword() {
  var raw = global.raw({
    password: $("#modal-change-password-new").val()
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/users/update/" + $("#id_m_users").val() + "?tokenize=" + global.csrf_token, function(res) {

        $("#btn-change-password-user").removeAttr("disabled");
        $("#btn-change-password-user").html(`Simpan`);

        if (res.code == 200) {

          $("#form-change-password-user")[0].reset();
          $("#modal-change-password-user").modal('toggle');
          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function uploadImage(image, callback) {
  var formData = new FormData();
  formData.append("photo", image);

  global.postRAW(formData, 
      global.base_url + "/services/medias/create?tokenize=" + global.csrf_token, function(res) {

        if (res.code == 200) {

          callback(res.data);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function loadUser() {
  var id = $("#id_m_users").val();
  global.getRAW(global.base_url + "/services/Users/get/" + id + "?tokenize=" + global.csrf_token, function(res) {
    if (res.code == 200) {
      var data = res.data;
      $("#full_name").val(data.full_name);
      $("#call_name").val(data.call_name);
      $("#gender").val(data.gender);
      $("#religion").val(data.religion);
      $("#phone_number").val(data.phone_number);
      $("#address").val(data.address);

      return
    }

    NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function doSaveUser() {
  var raw = global.raw({
      full_name: $("#full_name").val(),
      call_name: $("#call_name").val(),
      gender: $("#gender").val(),
      religion: $("#religion").val(),
      phone_number: $("#phone_number").val(),
      address: $("#address").val()
  });

  console.log(raw);

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Save Class
  global.postRAW(formData, 
      global.base_url + "/services/Users/update/" + $("#id_m_users").val() + "?tokenize=" + global.csrf_token, function(res) {

        $("#btn-save").removeAttr("disabled");
        $("#btn-save").html(`Simpan`);

        if (res.code == 200) {

          $("#form-save-user")[0].reset();
          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

          var setLoadUser = setInterval(() => {
            location.reload();
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}