"use strict";

var classNames = [];

!function (NioApp, $) {
  "use strict";

  $("#btn-create-class").on("click", function() {
    $("#btn-create-class").attr("disabled", true);
    $("#btn-create-class").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doCreateClass();
  });

  $("#btn-change-password-user").on("click", function() {
    $("#btn-change-password-user").attr("disabled", true);
    $("#btn-change-password-user").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doChangePassword();
  });

  $("#btn-save").on("click", function() {
    $("#btn-save").attr("disabled", true);
    $("#btn-save").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doSaveUser();
  });

  $("#btn-modal-change-photo").on("click", function() {
    $("#btn-modal-change-photo").attr("disabled", true);
    $("#btn-modal-change-photo").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doChangePhoto();
  });

  $("#modal-change-photo-input").change(function() {
    readURL(this);
  });

  loadClassName()

  loadUser();

}(NioApp, jQuery);

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#modal-change-photo-preview').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function doChangePhoto() {
  var file = $("#modal-change-photo-input").prop('files')[0];
  if (file == null) {
    $("#btn-modal-change-photo").removeAttr("disabled");
    $("#btn-modal-change-photo").html(`Simpan`);

    NioApp.Toast("Harap memilih foto terlebih dahulu", 'error', {position: 'bottom-right'});
    return
  }

  uploadImage(file, function(media) {
    var id_m_medias = media.id;
    var id = $("#id_m_users").val();
    var target = global.base_url + "/services/users/setUserFace/"+id+"?id_m_medias="+id_m_medias+"&tokenize=" + global.csrf_token
    // Do Load Konselor
    global.getRAW(
      target, function(res) {
        $("#btn-modal-change-photo").removeAttr("disabled");
        $("#btn-modal-change-photo").html(`Simpan`);

        if (res.code == 200) {

          $("#form-change-photo")[0].reset();
          $("#modal-change-photo").modal('toggle');
          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

          setTimeout(() => {
            location.reload();
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
  })
}

function setupClassName() {
  $("#class_name").html(`
  <option value="">Pilih Kelas</option>
  ${classNames.map(function(item) {
    return `<option value="${item.id}">${item.class_name} (${item.class_code})</option>`;
  }).join('')}`);
}

function loadClassName() {
  global.getRAW(global.base_url + "/services/className/all?tokenize=" + global.csrf_token, function(res) {
    if (res.code == 200) {
      classNames = res.data;
      setupClassName();
      return
    }

    NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function doCreateClass() {
    var raw = global.raw({
        class_name: $("#modal-create-class-class_name").val(),
        class_code: $("#modal-create-class-class_code").val()
    });

    var formData = new FormData()
    formData.append("raw", raw);

    // Do Create Class
    global.postRAW(formData, 
        global.base_url + "/services/className/create?tokenize=" + global.csrf_token, function(res) {

          $("#btn-create-class").removeAttr("disabled");
          $("#btn-create-class").html(`Simpan`);

          if (res.code == 200) {
  
            $("#form-create-class")[0].reset();
            $("#modal-create-class").modal('toggle');
            NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
            loadClassName()
  
            return
          }
  
          NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
        });
}

function doChangePassword() {
  var raw = global.raw({
    password: $("#modal-change-password-new").val()
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/users/update/" + $("#id_m_users").val() + "?tokenize=" + global.csrf_token, function(res) {

        $("#btn-change-password-user").removeAttr("disabled");
        $("#btn-change-password-user").html(`Simpan`);

        if (res.code == 200) {

          $("#form-change-password-user")[0].reset();
          $("#modal-change-password-user").modal('toggle');
          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function loadUser() {
  var id = $("#id_m_users").val();
  global.getRAW(global.base_url + "/services/Users/getConseli/" + id + "?tokenize=" + global.csrf_token, function(res) {
    if (res.code == 200) {
      var data = res.data;
      $("#full_name").val(data.full_name);
      $("#call_name").val(data.call_name);
      $("#gender").val(data.gender);
      $("#religion").val(data.religion);
      $("#phone_number").val(data.phone_number);
      $("#address").val(data.address);

      $("#class_name").val(data.utility.id_m_class);
      $("#nis").val(data.utility.nis);
      $("#date_of_birth").val(data.utility.date_of_birth);
      $("#place_of_birth").val(data.utility.place_of_birth);
      $("#living_together").val(data.utility.living_together);
      $("#distance_from_home").val(data.utility.distance_from_home);
      $("#vehicle").val(data.utility.vehicle);
      $("#child_position").val(data.utility.child_position);
      $("#brother_size").val(data.utility.brother_size);
      $("#brother_male_size").val(data.utility.brother_male_size);
      $("#brother_female_size").val(data.utility.brother_female_size);
      $("#litle_brother_male_size").val(data.utility.litle_brother_male_size);
      $("#litle_brother_female_size").val(data.utility.litle_brother_female_size);
      $("#hobby").val(data.utility.hobby);
      $("#history_of_disease").val(data.utility.history_of_disease);
      $("#interest").val(data.utility.interest);
      $("#problem").val(data.utility.problem);

      var parents = data.utility.parents;
      parents.forEach(function(item) {
        var type = item.type.toLowerCase();
        if (type == "ayah") type = "father";
        else if (type == "ibu") type = "mother";
        else if (type == "wali") type = "wali";

        $("#name_" + type).val(item.name);
        $("#religion_" + type).val(item.religion);
        $("#place_of_birth_" + type).val(item.place_of_birth);
        $("#date_of_birth_" + type).val(item.date_of_birth);
        $("#job_" + type).val(item.job);
        $("#address_" + type).val(item.address);
        $("#phone_number_" + type).val(item.phone_number);
      });

      return
    }

    NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function uploadImage(image, callback) {
  var formData = new FormData();
  formData.append("photo", image);

  global.postRAW(formData, 
      global.base_url + "/services/medias/create?tokenize=" + global.csrf_token, function(res) {

        if (res.code == 200) {

          callback(res.data);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function doSaveUser() {
  var full_name = $("#full_name").val(); // Wajib
  var call_name = $("#call_name").val();
  var gender = $("#gender").val(); // Wajib
  var religion = $("#religion").val(); // Wajib
  var phone_number = $("#phone_number").val();
  var address = $("#address").val();

  var id_m_class = $("#class_name").val();
  var nis = $("#nis").val();
  var date_of_birth = $("#date_of_birth").val();
  var place_of_birth = $("#place_of_birth").val();
  var living_together = $("#living_together").val();
  var distance_from_home = $("#distance_from_home").val();
  var vehicle = $("#vehicle").val();
  var child_position = $("#child_position").val();
  var brother_size = $("#brother_size").val();
  var brother_male_size = $("#brother_male_size").val();
  var brother_female_size = $("#brother_female_size").val();
  var litle_brother_male_size = $("#litle_brother_male_size").val();
  var litle_brother_female_size = $("#litle_brother_female_size").val();
  var hobby = $("#hobby").val();
  var history_of_disease = $("#history_of_disease").val();
  var interest = $("#interest").val();
  var problem = $("#problem").val();

  var name_father = $("#name_father").val();
  var religion_father = $("#religion_father").val();
  var place_of_birth_father = $("#place_of_birth_father").val();
  var date_of_birth_father = $("#date_of_birth_father").val();
  var job_father = $("#job_father").val();
  var address_father = $("#address_father").val();
  var phone_number_father = $("#phone_number_father").val();

  var name_mother = $("#name_mother").val();
  var religion_mother = $("#religion_mother").val();
  var place_of_birth_mother = $("#place_of_birth_mother").val();
  var date_of_birth_mother = $("#date_of_birth_mother").val();
  var job_mother = $("#job_mother").val();
  var address_mother = $("#address_mother").val();
  var phone_number_mother = $("#phone_number_mother").val();

  var name_wali = $("#name_wali").val();
  var religion_wali = $("#religion_wali").val();
  var place_of_birth_wali = $("#place_of_birth_wali").val();
  var date_of_birth_wali = $("#date_of_birth_wali").val();
  var job_wali = $("#job_wali").val();
  var address_wali = $("#address_wali").val();
  var phone_number_wali = $("#phone_number_wali").val();

  if (full_name == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nama lengkap", 'error', {position: 'bottom-right'});
    return;
  } else if (gender == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jenis kelamin", 'error', {position: 'bottom-right'});
    return;
  } else if (religion == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi agama", 'error', {position: 'bottom-right'});
    return;
  }

  if (id_m_class == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi kelas", 'error', {position: 'bottom-right'});
    return;
  } else if (nis == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nis", 'error', {position: 'bottom-right'});
    return;
  } else if (date_of_birth == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tanggal lahir", 'error', {position: 'bottom-right'});
    return;
  } else if (place_of_birth == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tempat lahir", 'error', {position: 'bottom-right'});
    return;
  } else if (living_together == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tinggal bersama siapa", 'error', {position: 'bottom-right'});
    return;
  } else if (distance_from_home == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jarak dari rumah ke sekolah", 'error', {position: 'bottom-right'});
    return;
  } else if (vehicle == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi berangkat sekolah dengan", 'error', {position: 'bottom-right'});
    return;
  } else if (child_position == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi anak ke", 'error', {position: 'bottom-right'});
    return;
  } else if (brother_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah bersaudara", 'error', {position: 'bottom-right'});
    return;
  } else if (brother_male_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah kakak laki-laki", 'error', {position: 'bottom-right'});
    return;
  } else if (brother_female_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah kakak perempuan", 'error', {position: 'bottom-right'});
    return;
  } else if (litle_brother_male_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah adik laki-laki", 'error', {position: 'bottom-right'});
    return;
  } else if (litle_brother_female_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah adik perempuan", 'error', {position: 'bottom-right'});
    return;
  }

  var parents = [];

  if (name_father != "") {
    parents.push({
      name: name_father,
      religion: religion_father,
      place_of_birth: place_of_birth_father,
      date_of_birth: date_of_birth_father,
      job: job_father,
      address: address_father,
      phone_number: phone_number_father,
      type: "Ayah"
    })
  } if (name_mother != "") {
    parents.push({
      name: name_mother,
      religion: religion_mother,
      place_of_birth: place_of_birth_mother,
      date_of_birth: date_of_birth_mother,
      job: job_mother,
      address: address_mother,
      phone_number: phone_number_mother,
      type: "Ibu"
    })
  } if (name_wali != "") {
    parents.push({
      name: name_wali,
      religion: religion_wali,
      place_of_birth: place_of_birth_wali,
      date_of_birth: date_of_birth_wali,
      job: job_wali,
      address: address_wali,
      phone_number: phone_number_wali,
      type: "Wali"
    })
  }

  var raw = global.raw({
    full_name: full_name,
    call_name: call_name,
    gender: gender,
    religion: religion,
    phone_number: phone_number,
    address: address,

    id_m_class: id_m_class,
    nis: nis,
    date_of_birth: date_of_birth,
    place_of_birth: place_of_birth,
    living_together: living_together,
    distance_from_home: distance_from_home,
    vehicle: vehicle,
    child_position: child_position,
    brother_size: brother_size,
    brother_male_size: brother_male_size,
    brother_female_size: brother_female_size,
    litle_brother_male_size: litle_brother_male_size,
    litle_brother_female_size: litle_brother_female_size,
    hobby: hobby,
    history_of_disease: history_of_disease,
    interest: interest,
    problem: problem,
    
    parents: parents
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/users/updateConseli/" + $("#id_m_users").val() + "?tokenize=" + global.csrf_token, function(res) {

        $("#btn-save").removeAttr("disabled");
        $("#btn-save").html(`Simpan`);

        if (res.code == 200) {

          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
          
          setTimeout(() => {
            location.reload();
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}