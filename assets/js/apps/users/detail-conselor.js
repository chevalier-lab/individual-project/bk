"use strict";

var classNames = [];
var classNamesConselor = [];

!function (NioApp, $) {
  "use strict";

  $("#btn-create-class").on("click", function() {
    $("#btn-create-class").attr("disabled", true);
    $("#btn-create-class").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doCreateClass();
  });

  $("#btn-change-password-user").on("click", function() {
    $("#btn-change-password-user").attr("disabled", true);
    $("#btn-change-password-user").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doChangePassword();
  });

  $("#btn-save").on("click", function() {
    $("#btn-save").attr("disabled", true);
    $("#btn-save").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doSaveUser();
  });

  $("#btn-modal-change-photo").on("click", function() {
    $("#btn-modal-change-photo").attr("disabled", true);
    $("#btn-modal-change-photo").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doChangePhoto();
  });

  $("#modal-change-photo-input").change(function() {
    readURL(this);
  });

  loadClassName()

  loadUser();

}(NioApp, jQuery);

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();
    
    reader.onload = function(e) {
      $('#modal-change-photo-preview').attr('src', e.target.result);
    }
    
    reader.readAsDataURL(input.files[0]); // convert to base64 string
  }
}

function doChangePhoto() {
  var file = $("#modal-change-photo-input").prop('files')[0];
  if (file == null) {
    $("#btn-modal-change-photo").removeAttr("disabled");
    $("#btn-modal-change-photo").html(`Simpan`);

    NioApp.Toast("Harap memilih foto terlebih dahulu", 'error', {position: 'bottom-right'});
    return
  }

  uploadImage(file, function(media) {
    var id_m_medias = media.id;
    var id = $("#id_m_users").val();
    var target = global.base_url + "/services/users/setUserFace/"+id+"?id_m_medias="+id_m_medias+"&tokenize=" + global.csrf_token
    // Do Load Konselor
    global.getRAW(
      target, function(res) {
        $("#btn-modal-change-photo").removeAttr("disabled");
        $("#btn-modal-change-photo").html(`Simpan`);

        if (res.code == 200) {

          $("#form-change-photo")[0].reset();
          $("#modal-change-photo").modal('toggle');
          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

          setTimeout(() => {
            location.reload();
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
  })
}

function setupClassName() {
  $("#class_name").html(`
  <option value="">Pilih Kelas</option>
  ${classNames.map(function(item) {
    return `<option value="${item.id}">${item.class_name} (${item.class_code})</option>`;
  }).join('')}`);

  $("#class_name_conselor").html(`
  <option value="">Pilih Kelas Bimbingan</option>
  ${classNames.map(function(item) {
    return `<option value="${item.id}">${item.class_name} (${item.class_code})</option>`;
  }).join('')}`);
}

function loadClassName() {
  global.getRAW(global.base_url + "/services/className/all?tokenize=" + global.csrf_token, function(res) {
    if (res.code == 200) {
      classNames = res.data;
      setupClassName();
      return
    }

    NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function doCreateClass() {
    var raw = global.raw({
        class_name: $("#modal-create-class-class_name").val(),
        class_code: $("#modal-create-class-class_code").val()
    });

    var formData = new FormData()
    formData.append("raw", raw);

    // Do Create Class
    global.postRAW(formData, 
        global.base_url + "/services/className/create?tokenize=" + global.csrf_token, function(res) {

          $("#btn-create-class").removeAttr("disabled");
          $("#btn-create-class").html(`Simpan`);

          if (res.code == 200) {
  
            $("#form-create-class")[0].reset();
            $("#modal-create-class").modal('toggle');
            NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
            loadClassName()
  
            return
          }
  
          NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
        });
}

function doChangePassword() {
  var raw = global.raw({
    password: $("#modal-change-password-new").val()
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/users/update/" + $("#id_m_users").val() + "?tokenize=" + global.csrf_token, function(res) {

        $("#btn-change-password-user").removeAttr("disabled");
        $("#btn-change-password-user").html(`Simpan`);

        if (res.code == 200) {

          $("#form-change-password-user")[0].reset();
          $("#modal-change-password-user").modal('toggle');
          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function loadUser() {
  var id = $("#id_m_users").val();
  global.getRAW(global.base_url + "/services/Users/getConselor/" + id + "?tokenize=" + global.csrf_token, function(res) {
    if (res.code == 200) {
      var data = res.data;
      $("#full_name").val(data.full_name);
      $("#call_name").val(data.call_name);
      $("#gender").val(data.gender);
      $("#religion").val(data.religion);
      $("#phone_number").val(data.phone_number);
      $("#address").val(data.address);

      $("#place_of_birth").val(data.conselor.place_of_birth)
      $("#date_of_birth").val(data.conselor.date_of_birth)
      $("#marital_status_conselor").val(data.conselor.marital_status)
      $("#last_education_conselor").val(data.conselor.last_education)
      
      $("#school_name_conselor").val(data.conselor.job.school_name)
      $("#school_address_conselor").val(data.conselor.job.school_address)
      $("#employee_status_conselor").val(data.conselor.job.employee_status)
      $("#nik_conselor").val(data.conselor.job.nik)
      $("#year_of_service_conselor").val(data.conselor.job.year_of_service)
      $("#additional_task_conselor").val(data.conselor.job.additional_task)


      var data = data.conselor.classes
      data.forEach(function(item) {
        classNamesConselor.push({
          id: item.id,
          class_name: item.class_name,
          class_code: item.class_code
        });
      });

      setupClassNameConselor();

      return
    }

    NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function add_class_conselor(id) {
  var status = false;
  var position = 0;
  var index = -1;
  classNamesConselor.forEach(function(item) {
    if (item.id == id) {
      status = true;
    }

    position++;
  });

  position = 0;
  classNames.forEach(function(item) {
    if (item.id == id) index = position;

    position++;
  });

  if (!status) {
    classNamesConselor.push(classNames[index]);
  }

  setupClassNameConselor();
}

function setupClassNameConselor() {
  $("#class_name_conselor_list").html(`${classNamesConselor.map(function(item, position) {
    return `<span class="badge badge-pill badge-primary"
    onclick="removeClassNameConselor(${position})">${item.class_name} (${item.class_code})</span>`;
  }).join('')}`);
}

function removeClassNameConselor(position) {
  classNamesConselor = classNamesConselor.splice(position, 1);
  setupClassNameConselor();
}

function uploadImage(image, callback) {
  var formData = new FormData();
  formData.append("photo", image);

  global.postRAW(formData, 
      global.base_url + "/services/medias/create?tokenize=" + global.csrf_token, function(res) {

        if (res.code == 200) {

          callback(res.data);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function doSaveUser() {
  var full_name = $("#full_name").val(); // Wajib
  var call_name = $("#call_name").val();
  var gender = $("#gender").val(); // Wajib
  var religion = $("#religion").val(); // Wajib
  var phone_number = $("#phone_number").val();
  var address = $("#address").val();

  var place_of_birth = $("#place_of_birth").val(); // Wajib
  var date_of_birth = $("#date_of_birth").val(); // Wajib
  var marital_status = $("#marital_status_conselor").val(); // Wajib
  var last_education = $("#last_education_conselor").val(); // Wajib
  
  var school_name = $("#school_name_conselor").val(); // Wajib
  var school_address = $("#school_address_conselor").val(); // Wajib
  var employee_status = $("#employee_status_conselor").val(); // Wajib
  var nik = $("#nik_conselor").val(); // Wajib
  var year_of_service = $("#year_of_service_conselor").val(); // Wajib
  var additional_task = $("#additional_task_conselor").val(); // Wajib

  if (full_name == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nama lengkap", 'error', {position: 'bottom-right'});
    return;
  } else if (gender == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jenis kelamin", 'error', {position: 'bottom-right'});
    return;
  } else if (religion == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi agama", 'error', {position: 'bottom-right'});
    return;
  } else if (place_of_birth == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tempat lahir", 'error', {position: 'bottom-right'});
    return;
  } else if (date_of_birth == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tanggal lahir", 'error', {position: 'bottom-right'});
    return;
  } else if (marital_status == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi status perkawinan", 'error', {position: 'bottom-right'});
    return;
  } else if (last_education == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi pendidikan terakhir", 'error', {position: 'bottom-right'});
    return;
  } else if (school_name == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nama sekolah", 'error', {position: 'bottom-right'});
    return;
  } else if (school_address == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi alamat sekolah", 'error', {position: 'bottom-right'});
    return;
  } else if (employee_status == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi status kepegawaian", 'error', {position: 'bottom-right'});
    return;
  } else if (nik == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nik", 'error', {position: 'bottom-right'});
    return;
  } else if (year_of_service == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi masa kerja", 'error', {position: 'bottom-right'});
    return;
  } else if (additional_task == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tugas tambahan", 'error', {position: 'bottom-right'});
    return;
  } else if (classNamesConselor.length == 0) {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi kelas bimbingan", 'error', {position: 'bottom-right'});
    return;
  }

  var raw = global.raw({
    full_name: full_name,
    call_name: call_name,
    gender: gender,
    religion: religion,
    phone_number: phone_number,
    address: address,

    place_of_birth: place_of_birth,
    date_of_birth: date_of_birth,
    marital_status: marital_status,
    last_education: last_education,

    school_name: school_name,
    school_address: school_address,
    employee_status: employee_status,
    nik: nik,
    year_of_service: year_of_service,
    additional_task: additional_task,

    class_names: classNamesConselor
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/users/updateConselor/" + $("#id_m_users").val() + "?tokenize=" + global.csrf_token, function(res) {

        $("#btn-save").removeAttr("disabled");
        $("#btn-save").html(`Simpan`);

        if (res.code == 200) {

          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
          
          setTimeout(() => {
            location.reload();
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}