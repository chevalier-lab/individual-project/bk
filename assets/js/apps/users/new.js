"use strict";

var classNames = [];
var classNamesConselor = [];

!function (NioApp, $) {
  "use strict";

  checkUserType("");

  $("#btn-create-class").on("click", function() {
    $("#btn-create-class").attr("disabled", true);
    $("#btn-create-class").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doCreateClass();
  });

  $("#btn-save").on("click", function() {
    $("#btn-save").attr("disabled", true);
    $("#btn-save").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);
    
    var type = $("#type").val();

    if (type == "") {
      $("#btn-save").removeAttr("disabled");
      $("#btn-save").html(`Simpan`);
      NioApp.Toast("Harap memilih jenis pengguna", 'error', {position: 'bottom-right'});
      return;
    } else if (type == "administrator") doCreateUserAdmin();
    else if (type == "konselor") doCreateUserConselor();
    else if (type == "konseli") doCreateUserConseli();
    else {
      $("#btn-save").removeAttr("disabled");
      $("#btn-save").html(`Simpan`);
      NioApp.Toast("Harap mengisi form", 'error', {position: 'bottom-right'});
    }

  });

}(NioApp, jQuery);

function checkUserType(type) {
    if (type == "konseli") {
        $("#container-additional-label").show();
        $("#container-additional").show();
        $("#container-additional-divider").show();
        
        $("#container-student-label").show();
        $("#container-student").show();
        $("#container-student-divider").show();
        $("#container-student-father-label").show();
        $("#container-student-father").show();
        $("#container-student-father-divider").show();
        $("#container-student-mother-label").show();
        $("#container-student-mother").show();
        $("#container-student-mother-divider").show();
        $("#container-student-wali-label").show();
        $("#container-student-wali").show();

        $("#container-conselor-label").hide();
        $("#container-conselor").hide();

        loadClassName();
    } else if (type == "konselor") {
      $("#container-additional-label").show();
      $("#container-additional").show();
      $("#container-additional-divider").show();
      
      $("#container-student-label").hide();
      $("#container-student").hide();
      $("#container-student-divider").hide();
      $("#container-student-father-label").hide();
      $("#container-student-father").hide();
      $("#container-student-father-divider").hide();
      $("#container-student-mother-label").hide();
      $("#container-student-mother").hide();
      $("#container-student-mother-divider").hide();
      $("#container-student-wali-label").hide();
      $("#container-student-wali").hide();

      $("#container-conselor-label").show();
      $("#container-conselor").show();

      loadClassName();
      setupClassNameConselor();
  } else {
    $("#container-additional-label").hide();
    $("#container-additional").hide();
    $("#container-additional-divider").hide();
    
    $("#container-student-label").hide();
    $("#container-student").hide();
    $("#container-student-divider").hide();
    $("#container-student-father-label").hide();
    $("#container-student-father").hide();
    $("#container-student-father-divider").hide();
    $("#container-student-mother-label").hide();
    $("#container-student-mother").hide();
    $("#container-student-mother-divider").hide();
    $("#container-student-wali-label").hide();
    $("#container-student-wali").hide();

    $("#container-conselor-label").hide();
    $("#container-conselor").hide();
  }
}

function add_class_conselor(id) {
  var status = false;
  var position = 0;
  var index = -1;
  classNamesConselor.forEach(function(item) {
    if (item.id == id) {
      status = true;
    }

    position++;
  });

  position = 0;
  classNames.forEach(function(item) {
    if (item.id == id) index = position;

    position++;
  });

  if (!status) {
    classNamesConselor.push(classNames[index]);
  }

  setupClassNameConselor();
}

function setupClassNameConselor() {
  $("#class_name_conselor_list").html(`${classNamesConselor.map(function(item, position) {
    return `<span class="badge badge-pill badge-primary"
    onclick="removeClassNameConselor(${position})">${item.class_name} (${item.class_code})</span>`;
  }).join('')}`);
}

function removeClassNameConselor(position) {
  classNamesConselor = classNamesConselor.splice(position, 1);
  setupClassNameConselor();
}

function setupClassName() {
  $("#class_name").html(`
  <option value="">Pilih Kelas</option>
  ${classNames.map(function(item) {
    return `<option value="${item.id}">${item.class_name} (${item.class_code})</option>`;
  }).join('')}`);

  $("#class_name_conselor").html(`
  <option value="">Pilih Kelas Bimbingan</option>
  ${classNames.map(function(item) {
    return `<option value="${item.id}">${item.class_name} (${item.class_code})</option>`;
  }).join('')}`);
}

function loadClassName() {
  global.getRAW(global.base_url + "/services/className/all?tokenize=" + global.csrf_token, function(res) {
    if (res.code == 200) {
      classNames = res.data;
      setupClassName();
      return
    }

    NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
  });
}

function doCreateClass() {
    var raw = global.raw({
        class_name: $("#modal-create-class-class_name").val(),
        class_code: $("#modal-create-class-class_code").val()
    });

    var formData = new FormData()
    formData.append("raw", raw);

    // Do Create Class
    global.postRAW(formData, 
        global.base_url + "/services/className/create?tokenize=" + global.csrf_token, function(res) {

          $("#btn-create-class").removeAttr("disabled");
          $("#btn-create-class").html(`Simpan`);

          if (res.code == 200) {
  
            $("#form-create-class")[0].reset();
            $("#modal-create-class").modal('toggle');
            NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
            loadClassName()
  
            return
          }
  
          NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
        });
}

function doCreateUserAdmin() { 
  var full_name = $("#full_name").val(); // Wajib
  var call_name = $("#call_name").val();
  var gender = $("#gender").val(); // Wajib
  var religion = $("#religion").val(); // Wajib
  var phone_number = $("#phone_number").val();
  var address = $("#address").val();
  
  var email = $("#email").val(); // Wajib
  var password = $("#password").val(); // Wajib

  if (full_name == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nama lengkap", 'error', {position: 'bottom-right'});
    return;
  } else if (gender == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jenis kelamin", 'error', {position: 'bottom-right'});
    return;
  } else if (religion == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi agama", 'error', {position: 'bottom-right'});
    return;
  } else if (email == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi email", 'error', {position: 'bottom-right'});
    return;
  } else if (password == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi password", 'error', {position: 'bottom-right'});
    return;
  }

  var raw = global.raw({
    full_name: full_name,
    call_name: call_name,
    gender: gender,
    religion: religion,
    phone_number: phone_number,
    address: address,
    email: email,
    password: password
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/users/createAdmin?tokenize=" + global.csrf_token, function(res) {

        $("#btn-save").removeAttr("disabled");
        $("#btn-save").html(`Simpan`);

        if (res.code == 200) {

          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
          
          setTimeout(() => {
            location.reload();
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function doCreateUserConselor() { 
  var full_name = $("#full_name").val(); // Wajib
  var call_name = $("#call_name").val();
  var gender = $("#gender").val(); // Wajib
  var religion = $("#religion").val(); // Wajib
  var phone_number = $("#phone_number").val();
  var address = $("#address").val();
  
  var email = $("#email").val(); // Wajib
  var password = $("#password").val(); // Wajib

  var place_of_birth = $("#place_of_birth").val(); // Wajib
  var date_of_birth = $("#date_of_birth").val(); // Wajib
  var marital_status = $("#marital_status_conselor").val(); // Wajib
  var last_education = $("#last_education_conselor").val(); // Wajib
  
  var school_name = $("#school_name_conselor").val(); // Wajib
  var school_address = $("#school_address_conselor").val(); // Wajib
  var employee_status = $("#employee_status_conselor").val(); // Wajib
  var nik = $("#nik_conselor").val(); // Wajib
  var year_of_service = $("#year_of_service_conselor").val(); // Wajib
  var additional_task = $("#additional_task_conselor").val(); // Wajib

  if (full_name == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nama lengkap", 'error', {position: 'bottom-right'});
    return;
  } else if (gender == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jenis kelamin", 'error', {position: 'bottom-right'});
    return;
  } else if (religion == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi agama", 'error', {position: 'bottom-right'});
    return;
  } else if (email == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi email", 'error', {position: 'bottom-right'});
    return;
  } else if (password == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi password", 'error', {position: 'bottom-right'});
    return;
  } else if (place_of_birth == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tempat lahir", 'error', {position: 'bottom-right'});
    return;
  } else if (date_of_birth == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tanggal lahir", 'error', {position: 'bottom-right'});
    return;
  } else if (marital_status == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi status perkawinan", 'error', {position: 'bottom-right'});
    return;
  } else if (last_education == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi pendidikan terakhir", 'error', {position: 'bottom-right'});
    return;
  } else if (school_name == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nama sekolah", 'error', {position: 'bottom-right'});
    return;
  } else if (school_address == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi alamat sekolah", 'error', {position: 'bottom-right'});
    return;
  } else if (employee_status == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi status kepegawaian", 'error', {position: 'bottom-right'});
    return;
  } else if (nik == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nik", 'error', {position: 'bottom-right'});
    return;
  } else if (year_of_service == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi masa kerja", 'error', {position: 'bottom-right'});
    return;
  } else if (additional_task == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tugas tambahan", 'error', {position: 'bottom-right'});
    return;
  } else if (classNamesConselor.length == 0) {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi kelas bimbingan", 'error', {position: 'bottom-right'});
    return;
  }

  var raw = global.raw({
    full_name: full_name,
    call_name: call_name,
    gender: gender,
    religion: religion,
    phone_number: phone_number,
    address: address,
    email: email,
    password: password,

    place_of_birth: place_of_birth,
    date_of_birth: date_of_birth,
    marital_status: marital_status,
    last_education: last_education,

    school_name: school_name,
    school_address: school_address,
    employee_status: employee_status,
    nik: nik,
    year_of_service: year_of_service,
    additional_task: additional_task,

    class_names: classNamesConselor
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/users/createConselor?tokenize=" + global.csrf_token, function(res) {

        $("#btn-save").removeAttr("disabled");
        $("#btn-save").html(`Simpan`);

        if (res.code == 200) {

          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
          
          setTimeout(() => {
            location.reload();
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function doCreateUserConseli() { 
  var full_name = $("#full_name").val(); // Wajib
  var call_name = $("#call_name").val();
  var gender = $("#gender").val(); // Wajib
  var religion = $("#religion").val(); // Wajib
  var phone_number = $("#phone_number").val();
  var address = $("#address").val();
  
  var email = $("#email").val(); // Wajib
  var password = $("#password").val(); // Wajib

  var id_m_class = $("#class_name").val();
  var nis = $("#nis").val();
  var date_of_birth = $("#date_of_birth").val();
  var place_of_birth = $("#place_of_birth").val();
  var living_together = $("#living_together").val();
  var distance_from_home = $("#distance_from_home").val();
  var vehicle = $("#vehicle").val();
  var child_position = $("#child_position").val();
  var brother_size = $("#brother_size").val();
  var brother_male_size = $("#brother_male_size").val();
  var brother_female_size = $("#brother_female_size").val();
  var litle_brother_male_size = $("#litle_brother_male_size").val();
  var litle_brother_female_size = $("#litle_brother_female_size").val();
  var hobby = $("#hobby").val();
  var history_of_disease = $("#history_of_disease").val();
  var interest = $("#interest").val();
  var problem = $("#problem").val();

  var name_father = $("#name_father").val();
  var religion_father = $("#religion_father").val();
  var place_of_birth_father = $("#place_of_birth_father").val();
  var date_of_birth_father = $("#date_of_birth_father").val();
  var job_father = $("#job_father").val();
  var address_father = $("#address_father").val();
  var phone_number_father = $("#phone_number_father").val();

  var name_mother = $("#name_mother").val();
  var religion_mother = $("#religion_mother").val();
  var place_of_birth_mother = $("#place_of_birth_mother").val();
  var date_of_birth_mother = $("#date_of_birth_mother").val();
  var job_mother = $("#job_mother").val();
  var address_mother = $("#address_mother").val();
  var phone_number_mother = $("#phone_number_mother").val();

  var name_wali = $("#name_wali").val();
  var religion_wali = $("#religion_wali").val();
  var place_of_birth_wali = $("#place_of_birth_wali").val();
  var date_of_birth_wali = $("#date_of_birth_wali").val();
  var job_wali = $("#job_wali").val();
  var address_wali = $("#address_wali").val();
  var phone_number_wali = $("#phone_number_wali").val();

  if (full_name == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nama lengkap", 'error', {position: 'bottom-right'});
    return;
  } else if (gender == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jenis kelamin", 'error', {position: 'bottom-right'});
    return;
  } else if (religion == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi agama", 'error', {position: 'bottom-right'});
    return;
  } else if (email == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi email", 'error', {position: 'bottom-right'});
    return;
  } else if (password == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi password", 'error', {position: 'bottom-right'});
    return;
  }

  if (id_m_class == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi kelas", 'error', {position: 'bottom-right'});
    return;
  } else if (nis == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi nis", 'error', {position: 'bottom-right'});
    return;
  } else if (date_of_birth == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tanggal lahir", 'error', {position: 'bottom-right'});
    return;
  } else if (place_of_birth == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tempat lahir", 'error', {position: 'bottom-right'});
    return;
  } else if (living_together == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi tinggal bersama siapa", 'error', {position: 'bottom-right'});
    return;
  } else if (distance_from_home == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jarak dari rumah ke sekolah", 'error', {position: 'bottom-right'});
    return;
  } else if (vehicle == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi berangkat sekolah dengan", 'error', {position: 'bottom-right'});
    return;
  } else if (child_position == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi anak ke", 'error', {position: 'bottom-right'});
    return;
  } else if (brother_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah bersaudara", 'error', {position: 'bottom-right'});
    return;
  } else if (brother_male_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah kakak laki-laki", 'error', {position: 'bottom-right'});
    return;
  } else if (brother_female_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah kakak perempuan", 'error', {position: 'bottom-right'});
    return;
  } else if (litle_brother_male_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah adik laki-laki", 'error', {position: 'bottom-right'});
    return;
  } else if (litle_brother_female_size == "") {
    $("#btn-save").removeAttr("disabled");
    $("#btn-save").html(`Simpan`);
    NioApp.Toast("Harap mengisi jumlah adik perempuan", 'error', {position: 'bottom-right'});
    return;
  }

  var parents = [];

  if (name_father != "") {
    parents.push({
      name: name_father,
      religion: religion_father,
      place_of_birth: place_of_birth_father,
      date_of_birth: date_of_birth_father,
      job: job_father,
      address: address_father,
      phone_number: phone_number_father,
      type: "Ayah"
    })
  } if (name_mother != "") {
    parents.push({
      name: name_mother,
      religion: religion_mother,
      place_of_birth: place_of_birth_mother,
      date_of_birth: date_of_birth_mother,
      job: job_mother,
      address: address_mother,
      phone_number: phone_number_mother,
      type: "Ibu"
    })
  } if (name_wali != "") {
    parents.push({
      name: name_wali,
      religion: religion_wali,
      place_of_birth: place_of_birth_wali,
      date_of_birth: date_of_birth_wali,
      job: job_wali,
      address: address_wali,
      phone_number: phone_number_wali,
      type: "Wali"
    })
  }

  var raw = global.raw({
    full_name: full_name,
    call_name: call_name,
    gender: gender,
    religion: religion,
    phone_number: phone_number,
    address: address,
    email: email,
    password: password,

    id_m_class: id_m_class,
    nis: nis,
    date_of_birth: date_of_birth,
    place_of_birth: place_of_birth,
    living_together: living_together,
    distance_from_home: distance_from_home,
    vehicle: vehicle,
    child_position: child_position,
    brother_size: brother_size,
    brother_male_size: brother_male_size,
    brother_female_size: brother_female_size,
    litle_brother_male_size: litle_brother_male_size,
    litle_brother_female_size: litle_brother_female_size,
    hobby: hobby,
    history_of_disease: history_of_disease,
    interest: interest,
    problem: problem,
    
    parents: parents
  });

  var formData = new FormData()
  formData.append("raw", raw);

  // Do Create Class
  global.postRAW(formData, 
      global.base_url + "/services/users/createConseli?tokenize=" + global.csrf_token, function(res) {

        $("#btn-save").removeAttr("disabled");
        $("#btn-save").html(`Simpan`);

        if (res.code == 200) {

          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
          
          setTimeout(() => {
            location.reload();
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}