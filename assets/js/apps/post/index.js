"use strict";

var table = null;

!function (NioApp, $) {
  "use strict";

    loadPost();

}(NioApp, jQuery);

function loadPost() 
{
    if (table != null) {
        table.destroy();
    }

    table = NioApp.DataTable('.post-list', {
        responsive: {
            details: true
        },            
        processing: true,            
        serverSide: true,            
        ordering: true, 
        order: [[ 0, 'desc' ]], 
        ajax: {                
            url: global.base_url + "/services/post/index?tokenize=" + global.csrf_token, 
            type: "POST"
        },
        deferRender: true,            
        aLengthMenu: [[5, 10, 50],[ 5, 10, 50]],
        columns: [
            {
                render: function(data, type, row) {
                    return `
                    <div class="nk-block-image w-120px flex-shrink-0">
                        <img src="${row.uri}" style="max-width: 100%" />
                    </div>`;
                }
            },
            {
                render: function(data, type, row) {
                    return (row.title.length > 32) ? row.title.substring(0, 32) : row.title;
                }
            },
            {data: "category"},
            {
                render: function(data, type, row) {
                    return global.date(row.updated_at)
                }
            },
            {
                render: function(data, type, row) {
                    var target = global.base_url + "/views/homeDetail/" + row.id + "?tokenize=" + global.csrf_token
                    return `
                        <button class="btn btn-primary btn-sm" type="button"
                        onclick="location.assign('${target}')">
                            <em class="icon ni ni-eye-fill"></em> 
                            <span>Lihat</span>
                        </button>
                    `;
                }
            }
        ]
    });
    $.fn.DataTable.ext.pager.numbers_length = 7;
}