"use strict";

var categories = [];

!function (NioApp, $) {
  "use strict";

  loadCategory();

  $("#btn-create-category").on("click", function() {
    $("#btn-create-category").attr("disabled", true);
    $("#btn-create-category").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doCreateCategory();
  });

  $("#btn-save").on("click", function() {
    $("#btn-save").attr("disabled", true);
    $("#btn-save").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    doCreateArticle();
  });

    $('#article-content').summernote({
        height: "300px",
        toolbar: [
            // [groupName, [list of button]]
            ['style', ['bold', 'italic', 'underline', 'clear']],
            ['font', ['strikethrough', 'superscript', 'subscript']],
            ['fontsize', ['fontsize']],
            ['color', ['color']],
            ['para', ['ul', 'ol', 'paragraph']],
            ['height', ['height']]
        ]
    });

}(NioApp, jQuery);

function setupCategory() {
    $("#article-category").html(`${categories.map(function(item) {
        return `<option value="${item.id}">${item.category}</option>`;
    }).join('')}`);
}

function loadCategory() {
    global.getRAW(global.base_url + "/services/category/all?tokenize=" + global.csrf_token, function(res) {
        if (res.code == 200) {
          categories = res.data;
          setupCategory();
          return
        }
    
        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
      });
}

function doCreateCategory() {
    var raw = global.raw({
        category: $("#modal-create-category-name").val()
    });

    var formData = new FormData()
    formData.append("raw", raw);

    // Do Create Class
    global.postRAW(formData, 
        global.base_url + "/services/category/create?tokenize=" + global.csrf_token, function(res) {

          $("#btn-create-category").removeAttr("disabled");
          $("#btn-create-category").html(`Simpan`);

          if (res.code == 200) {
  
            $("#form-create-category")[0].reset();
            $("#modal-create-category").modal('toggle');
            NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
            loadCategory()
  
            return
          }
  
          NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
        });
}

function uploadImage(image, callback) {
    var formData = new FormData();
    formData.append("photo", image);

    global.postRAW(formData, 
        global.base_url + "/services/medias/create?tokenize=" + global.csrf_token, function(res) {
  
          if (res.code == 200) {
  
            callback(res.data);
  
            return
          }
  
          NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
        });
}

function doCreateArticle() {
    var title = $("#article-title").val();
    var content = $("#article-content").summernote('code');
    var category = $("#article-category").val();

    if (title == "") {
      $("#btn-save").removeAttr("disabled");
      $("#btn-save").html(`Simpan`);
      NioApp.Toast("Harap mengisi judul artikel terlebih dahulu", 'error', {position: 'bottom-right'});
      return;
    } else if (content == "" || content == "<p></p>") {
      $("#btn-save").removeAttr("disabled");
      $("#btn-save").html(`Simpan`);
      NioApp.Toast("Harap mengisi konten artikel terlebih dahulu", 'error', {position: 'bottom-right'});
      return;
    } else if (category == "") {
      $("#btn-save").removeAttr("disabled");
      $("#btn-save").html(`Simpan`);
      NioApp.Toast("Harap memilih atau membuat kategori terlebih dahulu", 'error', {position: 'bottom-right'});
      return;
    } else if ($("#article-cover").prop("files").length == 0) {
      $("#btn-save").removeAttr("disabled");
      $("#btn-save").html(`Simpan`);
      NioApp.Toast("Harap memilih cover artikel terlebih dahulu", 'error', {position: 'bottom-right'});
      return;
    }

    uploadImage($("#article-cover").prop("files")[0], function(media) {
        var raw = global.raw({
            title: $("#article-title").val(),
            content: $("#article-content").summernote('code'),
            id_m_categories: $("#article-category").val(),
            id_m_medias: media.id
        });

        var formData = new FormData();
        formData.append("raw", raw);

        global.postRAW(formData, 
            global.base_url + "/services/post/create?tokenize=" + global.csrf_token, function(res) {

                $("#btn-save").removeAttr("disabled");
                $("#btn-save").html(`Simpan`);
    
                if (res.code == 200) {
        
                    NioApp.Toast(res.message, 'success', {position: 'bottom-right'});

                    setTimeout(() => {
                      location.reload();
                    }, 1000);
                    
                    return
                }
    
                NioApp.Toast(res.message, 'error', {position: 'bottom-right'});
            });
    })
}

