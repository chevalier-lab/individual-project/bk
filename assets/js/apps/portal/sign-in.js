"use strict";

!function (NioApp, $) {
  "use strict";

  console.log("TESTING");

  $("#btn-sign-in").on("click",function() {
    $("#btn-sign-in").attr("disabled", true);
    $("#btn-sign-in").html(`
        <span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>
        <span> Loading... </span>
    `);

    var userID = $("#user-id").val()
    var password = $("#password").val()

    var raw = global.raw({
      user_id: userID,
      password: password
    })

    var formData = new FormData();
    formData.append("raw", raw);

    // Do Login
    global.postRAW(formData, 
      global.base_url + "/services/portal/signIn?tokenize=" + global.csrf_token, function(res) {
        if (res.code == 200) {

          NioApp.Toast(res.message, 'success', {position: 'bottom-right'});
          
          //  Set Auth
          setTimeout(function() {
              var auth = global.raw(res.data)
              location.assign(global.base_url + "/views/set_auth?raw=" + auth)
          }, 1000);

          return
        }

        NioApp.Toast(res.message, 'error', {position: 'bottom-right'});

        $("#btn-sign-in").removeAttr("disabled");
        $("#btn-sign-in").html(`Masuk`);
      });
  });
}(NioApp, jQuery);